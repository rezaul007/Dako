<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;


class Deliveries extends Model
{
    public function delivery_info()
    {
        $user = DB::SELECT('SELECT * FROM delivery');
        return $user;
    }
    public function delivery_details($id)
    {
        $user = DB::SELECT("SELECT * FROM delivery WHERE id=$id");
        return $user;
    }
    public function detail_delete($id)
    {
        $query=DB::table('delivery')
            ->where('id','=',$id)
            ->delete();
        return $query;
    }
}
