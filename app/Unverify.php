<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;


class Unverify extends Model
{
    public function verify_info()
    {
        $user = DB::table('biker')
            ->where('biker.unverify', 0)
            ->select(DB::raw('*'))
            ->orderBy('biker.id','ASC')
            ->get();
        return $user;
    }
    public function edit_unverify($id){
        $info=DB::select("select * from `biker` where id=$id and unverify=0 ");
        return $info;
    }
    public function update_unverify($biker_name,$phone,$area,$gender,$nid,$reg,$location,$img,$model,$call_biker,$call_from,$test_result,$id){
        $info= DB::table('biker')
            ->where('biker.id', $id)
            ->update(['biker.biker_name' => $biker_name,'biker.biker_phn' => $phone,'biker.area' => $area,'biker.gender' => $gender,
                'biker.biker_nid' => $nid,'biker.biker_reg' => $reg,'biker.location' => $location,'biker.biker_img' => $img,
                'biker.bike_model' => $model,'biker.call_biker' => $call_biker,'biker.call_from' => $call_from,'biker.test_result' => $test_result]);
        return $info;
    }
    public function delete_unverify($id){
        $info=DB::select("DELETE FROM `biker` where id=$id and unverify=0 ");
        return $info;
    }
}
