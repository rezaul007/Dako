<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;


class Settings extends Model
{
    public function sms_api()
    {
        $user = DB::table('sms_api_setup')
            ->select(DB::raw('*'))
            ->orderBy('id','ASC')
            ->get();
        return $user;
    }
    public function update_sms($id,$total_sms,$unused_sms){
        $info = DB::table('sms_api_setup')
            ->where('id', $id)
            ->update(['total_sms' => $total_sms,'unused_sms' => $unused_sms]);
        return $info;
    }
    public function tracking_area(){
        $user = DB::table('tracking_area')
            ->select(DB::raw('*'))
            ->orderBy('id','ASC')
            ->get();
        return $user;
    }
    public function ride_request(){
        $user = DB::table('ride_request_accept')
            ->select(DB::raw('*'))
            ->orderBy('id','ASC')
            ->get();
        return $user;
    }
    public function update_tracking_area($id,$area){
        $info = DB::table('tracking_area')
            ->where('id', $id)
            ->update(['area' => $area]);
        return $info;
    }
    public function pick_time_info(){
        $user = DB::table('pick_off_pick')
            ->select(DB::raw('*'))
            ->orderBy('id','ASC')
            ->get();
        return $user;
    }
    public function update_schedule($id,$schedule){
//        echo '<pre>';var_dump($id);die;
        $info = DB::table('pick_off_pick')
            ->where('id', $id[1])
            ->update(['schedule' => $schedule[1]]);
        $info2 = DB::table('pick_off_pick')
            ->where('id', $id[2])
            ->update(['schedule' => $schedule[2]]);
        $info3 = DB::table('pick_off_pick')
            ->where('id', $id[3])
            ->update(['schedule' => $schedule[3]]);
        $info4 = DB::table('pick_off_pick')
            ->where('id', $id[4])
            ->update(['schedule' => $schedule[4]]);
        $info5 = DB::table('pick_off_pick')
            ->where('id', $id[5])
            ->update(['schedule' => $schedule[5]]);
        return $info;
    }
    public function update_tk($id,$tk,$per_min_rate,$average_time_status){
//        echo '<pre>';
//        var_dump($id);echo '<br>';var_dump($tk);die;
        $info = DB::table('pick_off_pick')
            ->where('id', $id[1])
            ->update(['tk' => filter_var($tk[1], FILTER_SANITIZE_NUMBER_INT)]);
        $info2 = DB::table('pick_off_pick')
            ->where('id', $id[2])
            ->update(['tk' => filter_var($tk[2], FILTER_SANITIZE_NUMBER_INT)]);
        $info3 = DB::table('pick_off_pick')
            ->where('id', $id[3])
            ->update(['tk' => filter_var($tk[3], FILTER_SANITIZE_NUMBER_INT)]);
        $info4 = DB::table('pick_off_pick')
            ->where('id', $id[4])
            ->update(['tk' => filter_var($tk[4], FILTER_SANITIZE_NUMBER_INT)]);
        $info5 = DB::table('pick_off_pick')
            ->where('id', $id[5])
            ->update(['tk' => filter_var($tk[5], FILTER_SANITIZE_NUMBER_INT)]);
        $per_min=(float) filter_var( $per_min_rate, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
        $info6 = DB::SELECT("UPDATE `pick_off_pick` SET `per_min_rate_non_average` = '$per_min' ");

        if($average_time_status!=null) {
            $info6 = DB::SELECT("UPDATE `pick_off_pick` SET `average_time_status` = 0 ");
        }
        return $info;
    }
    public function update_average_tk($average_tk,$per_min_rate,$average_time_status){
//        echo '<pre>';
//        var_dump($id);echo '<br>';var_dump($tk);die;
        $tk=filter_var($average_tk, FILTER_SANITIZE_NUMBER_INT);
        $per_min=(float) filter_var( $per_min_rate, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
        $info=DB::SELECT("UPDATE pick_off_pick SET `average_tk`='$tk'");
        $info6 = DB::SELECT("UPDATE `pick_off_pick` SET `per_min_rate_average` = '$per_min' ");
        if($average_time_status!=null) {
            $info6 = DB::SELECT("UPDATE `pick_off_pick` SET `average_time_status` = 1 ");
        }
        return $info;
    }
    public function update_ride_request($id,$area,$time){
//        echo '<pre>';var_dump($id);die;
        for($i=1;$i<count($id);$i++){
            $info = DB::table('ride_request_accept')
                ->where('id', $id[$i])
                ->update(['area' => $area[$i],'time' => $time[$i]]);
        }
        return $info;
    }
    public function refaral_how_many(){
            $user = DB::table('refaral_how_many')
                ->select(DB::raw('refaral_how_many.id,refaral_how_many.average_status,refaral_how_many.first,refaral_how_many.after,refaral_how_many.average,ride_request_accept.area'))
                ->join('ride_request_accept', 'ride_request_accept.id', '=', 'refaral_how_many.ride_request_for')
                ->orderBy('refaral_how_many.id','ASC')
                ->get();
            return $user;
    }
    public function update_non_average_refaral($id,$first,$after,$average_status){

        for($i=1;$i<count($first);$i++){
                $info = DB::table('refaral_how_many')
                    ->where('id', $id[$i])
                    ->update(['first' => $first[$i],'after' => $after[$i],'average_status' => $average_status[$i]]);
            }
        return $info;
        
    }
    public function update_average_refaral($id,$average,$average_status){
        for($i=1;$i<count($id);$i++){
            $info = DB::table('refaral_how_many')
                ->where('id', $id[$i])
                ->update(['average' => $average[$i],'average_status' => $average_status[$i]]);
        }
        return $info;
    }
    public function request_count_stage(){
        $info=DB::SELECT("SELECT * FROM request_count_stage");
        return $info;
    }public function account_info(){
        $info=DB::SELECT("SELECT * FROM account_info");
        return $info;
    }
    public function update_request_stage($id){
        $info=DB::SELECT("SELECT * FROM request_count_stage WHERE active_status=1");
        foreach ($info as $id_info){
            $set_id=$id_info->id;
        }
        $info = DB::table('request_count_stage')
            ->where('id', $set_id)
            ->update(['active_status' => '0']);

        $info = DB::table('request_count_stage')
            ->where('id', $id)
            ->update(['active_status' => '1']);

    }
    public function update_account_info($id,$account_name,$account_no){
        for($i=1;$i<count($id);$i++){
            $info = DB::table('account_info')
                ->where('id', $id[$i])
                ->update(['account_name' => $account_name[$i],'account_no' => $account_no[$i]]);
        }
        return $info;
    }
    public function search_person($info){
        $user = DB::SELECT("SELECT biker.id ,biker.biker_name,biker.biker_reg as info1,biker_phn as info2
                FROM `biker` WHERE biker.biker_reg LIKE '%$info%' OR biker.biker_phn LIKE '%$info%' ");
       if($user!=null){
           return $user;
       }
        else{
            $user = DB::SELECT("SELECT passenger.id,passenger.passenger_name,passenger.passenger_phn as info
                FROM `passenger` WHERE passenger.`passenger_phn` 
                LIKE '%$info%'");
            return $user;
        }
    }
    public function block_person($info){
        $user = DB::SELECT("SELECT biker.id ,biker.biker_name,biker.biker_reg as info1,biker_phn as info2
                FROM `biker` WHERE biker.biker_reg LIKE '%$info%' OR biker.biker_phn LIKE '%$info%' ");
        if($user!=null){
            foreach ($user as $person){
                if (array_key_exists('info1', $person)){
                    $i=$person->info1;
                    $user2 = DB::SELECT("UPDATE `biker` SET biker.block_info=1 WHERE biker.biker_reg= '$i' ");
                }
                if(array_key_exists('info2', $person)){
                    $i=$person->info2;
                    $user2 = DB::SELECT("UPDATE `biker` SET biker.block_info=1 WHERE biker.biker_phn= '$i' ");
                }
            }

            return $user2;
        }
        else {

            $user = DB::SELECT("SELECT passenger.id,passenger.passenger_name,passenger.passenger_phn as info
                FROM `passenger` WHERE passenger.`passenger_phn` 
                LIKE '%$info%'");
            foreach ($user as $person) {
                if (array_key_exists('info', $person)) {
                    $i = $person->info;
                    $user2 = DB::SELECT("UPDATE `passenger` SET passenger.block_info=1 WHERE passenger.passenger_phn= '$i' ");
                }
                return $user2;
            }
        }
    }
    public function insert_faq($qstn,$correct_ansr,$ansr1,$ansr2,$ansr3){
        $user=DB::SELECT("INSERT INTO `fqa_qstn` (`id`, `question`, `correct_answer`,`option_1`,`option_2`,`option_3`) VALUES (NULL, '$qstn', '$correct_ansr','$ansr1','$ansr2','$ansr3');");
        return $user;
    }
    public function whole_site_info(){
        $user = DB::table('whole_site_settings')
            ->select(DB::raw('*'))
            ->orderBy('id','ASC')
            ->get();
        return $user;
    }
    public function update_whole_site_settings($id,$posiotion_active,$position,$img,$link){
        $info = DB::table('whole_site_settings')
            ->where('id', $id)
            ->update(['position' => $position,'sitelink_or_script' => $link,'img' => $img,'active_status' => $posiotion_active]);

        return $info;
    }
    public function person_img_check($name){
        $info = DB::table('chat_person')
            ->where('img', $name)
            ->select(DB::raw('img'))
            ->get();
        return $info;
    }
    public function check_img($id){
        $info = DB::table('whole_site_settings')
            ->where('id', $id)
            ->select(DB::raw('img'))
        ->get();
        return $info;
    }
    public function check_site_n_logo_img($id){
        $info = DB::table('site_img_n_logo')
            ->where('id', $id)
            ->select(DB::raw('img'))
            ->get();
        return $info;
    }
    public function site_img_n_logo(){
        $user = DB::table('site_img_n_logo')
            ->select(DB::raw('*'))
            ->orderBy('id','ASC')
            ->get();
        return $user;
    }
    public function update_site_image_n_logo($id,$dialougue,$img){
        $info = DB::table('site_img_n_logo')
            ->where('id', $id)
            ->update(['dialougue' => $dialougue,'img' => $img]);

        return $info;
    }
    public function update_auto_chat($msg){
        $info = DB::SELECT("INSERT INTO `desktop_auto_chat` (`id`, `message`) VALUES (NULL, '$msg');");

        return $info;
    }
    public function update_chat_person($img_1,$img_2,$img_3){
        $info = DB::SELECT("SELECT img from chat_person WHERE id='1'");
        if(!$info){
            $info = DB::SELECT("INSERT INTO `chat_person` (`id`, `img`) VALUES (1, '$img_1');");
            
        }
        else{
            $id=1;
            $info = DB::table('chat_person')
                ->where('id', $id)
                ->update(['img' => $img_1]);
        }
        $info = DB::SELECT("SELECT img from chat_person WHERE id='2'");
        if(!$info){
            $info = DB::SELECT("INSERT INTO `chat_person` (`id`, `img`) VALUES (2, '$img_2');");

        }
        else{
            $id=2;
            $info = DB::table('chat_person')
                ->where('id', $id)
                ->update(['img' => $img_2]);
        }
        $info = DB::SELECT("SELECT img from chat_person WHERE id='3'");
        if(!$info){
            $info = DB::SELECT("INSERT INTO `chat_person` (`id`, `img`) VALUES (3, '$img_3');");

        }
        else{
            $id=3;
            $info = DB::table('chat_person')
                ->where('id', $id)
                ->update(['img' => $img_3]);
        }
    }
    public function dhaka_hours(){
        $user = DB::table('delivery_based_settings')
            ->select(DB::raw('hour_1,hour_2'))
            ->where('city','Dhaka')
            ->get();
        return $user;
    }
    public function dhaka_kg(){
        $user = DB::table('delivery_based_settings')
            ->select(DB::raw('kg_1_for_hr_1,kg_2_for_hr_1,kg_3_for_hr_1,kg_1_for_hr_2,kg_2_for_hr_2,kg_3_for_hr_2'))
            ->where('city','Dhaka')
            ->get();
        return $user;
    }
    public function dhaka_val(){
        $user = DB::table('delivery_based_settings')
            ->select(DB::raw('kg_val_1_for_hr_1,kg_val_2_for_hr_1,kg_val_3_for_hr_1,kg_val_1_for_hr_2,kg_val_2_for_hr_2,kg_val_3_for_hr_2'))
            ->where('city','Dhaka')
            ->get();
        return $user;
    }
    public function update_dhaka_hours($hr1,$hr2){
        $info = DB::table('delivery_based_settings')
            ->where('city', 'Dhaka')
            ->update(['hour_1' => $hr1,'hour_2' => $hr2]);
        return $info;        
    }
    public function update_dhaka_kg($kg1,$kg2,$kg3,$kg4,$kg5,$kg6){
        $info = DB::table('delivery_based_settings')
            ->where('city', 'Dhaka')
            ->update(['kg_1_for_hr_1' => $kg1,'kg_2_for_hr_1' => $kg2,'kg_3_for_hr_1' => $kg3,
                'kg_1_for_hr_2' => $kg4,'kg_2_for_hr_2' => $kg5,'kg_3_for_hr_2' => $kg6
            ]);
        return $info;
    }
    public function update_dhaka_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6){
        $info = DB::table('delivery_based_settings')
            ->where('city', 'Dhaka')
            ->update(['kg_val_1_for_hr_1' => $kg1,'kg_val_2_for_hr_1' => $kg2,'kg_val_3_for_hr_1' => $kg3,
                'kg_val_1_for_hr_2' => $kg4,'kg_val_2_for_hr_2' => $kg5,'kg_val_3_for_hr_2' => $kg6
            ]);
        return $info;
    }
    public function count_city(){
        $info=DB::SELECT("SELECT COUNT(*) as city FROM `delivery_based_settings` WHERE `city` !='Dhaka' ");
        return $info;
    }
    public function other_city_hours(){
        $user = DB::table('delivery_based_settings')
            ->select(DB::raw('hour_1,hour_2'))
            ->where('city','!=','Dhaka')
            ->get();
        return $user;
    }
    public function other_city_kg(){
        $user = DB::table('delivery_based_settings')
            ->select(DB::raw('kg_1_for_hr_1,kg_2_for_hr_1,kg_3_for_hr_1,kg_1_for_hr_2,kg_2_for_hr_2,kg_3_for_hr_2'))
            ->where('city','!=','Dhaka')
            ->get();
        return $user;
    }
    public function ctg_city_val(){
        $user = DB::table('delivery_based_settings')
            ->select(DB::raw('city,kg_val_1_for_hr_1,kg_val_2_for_hr_1,kg_val_3_for_hr_1,kg_val_1_for_hr_2,kg_val_2_for_hr_2,kg_val_3_for_hr_2'))
            ->where('city','CTG')
            ->get();
        return $user;
    }
    public function raj_city_val(){
        $user = DB::table('delivery_based_settings')
            ->select(DB::raw('city,kg_val_1_for_hr_1,kg_val_2_for_hr_1,kg_val_3_for_hr_1,kg_val_1_for_hr_2,kg_val_2_for_hr_2,kg_val_3_for_hr_2'))
            ->where('city','RAJ')
            ->get();
        return $user;
    }
    public function khul_city_val(){
        $user = DB::table('delivery_based_settings')
            ->select(DB::raw('city,kg_val_1_for_hr_1,kg_val_2_for_hr_1,kg_val_3_for_hr_1,kg_val_1_for_hr_2,kg_val_2_for_hr_2,kg_val_3_for_hr_2'))
            ->where('city','KHUL')
            ->get();
        return $user;
    }
    public function syl_city_val(){
        $user = DB::table('delivery_based_settings')
            ->select(DB::raw('city,kg_val_1_for_hr_1,kg_val_2_for_hr_1,kg_val_3_for_hr_1,kg_val_1_for_hr_2,kg_val_2_for_hr_2,kg_val_3_for_hr_2'))
            ->where('city','SYL')
            ->get();
        return $user;
    }
    public function overall_city_val(){
        $user = DB::table('delivery_based_settings')
            ->select(DB::raw('city,kg_val_1_for_hr_1,kg_val_2_for_hr_1,kg_val_3_for_hr_1,kg_val_1_for_hr_2,kg_val_2_for_hr_2,kg_val_3_for_hr_2'))
            ->where('city','Whole Bangladesh')
            ->get();
        return $user;
    }
    public function update_other_hours($hr1,$hr2){
        $info = DB::table('delivery_based_settings')
            ->where('city','!=', 'Dhaka')
            ->update(['hour_1' => $hr1,'hour_2' => $hr2]);
        return $info;
    }
    public function update_other_kg($kg1,$kg2,$kg3,$kg4,$kg5,$kg6){
        $info = DB::table('delivery_based_settings')
            ->where('city','!=', 'Dhaka')
            ->update(['kg_1_for_hr_1' => $kg1,'kg_2_for_hr_1' => $kg2,'kg_3_for_hr_1' => $kg3,
                'kg_1_for_hr_2' => $kg4,'kg_2_for_hr_2' => $kg5,'kg_3_for_hr_2' => $kg6
            ]);
        return $info;
    }
    public function update_ctg_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6){
        $info = DB::table('delivery_based_settings')
            ->where('city', 'CTG')
            ->update(['kg_val_1_for_hr_1' => $kg1,'kg_val_2_for_hr_1' => $kg2,'kg_val_3_for_hr_1' => $kg3,
                'kg_val_1_for_hr_2' => $kg4,'kg_val_2_for_hr_2' => $kg5,'kg_val_3_for_hr_2' => $kg6
            ]);
        return $info;
    }
    public function update_raj_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6){
        $info = DB::table('delivery_based_settings')
            ->where('city', 'RAJ')
            ->update(['kg_val_1_for_hr_1' => $kg1,'kg_val_2_for_hr_1' => $kg2,'kg_val_3_for_hr_1' => $kg3,
                'kg_val_1_for_hr_2' => $kg4,'kg_val_2_for_hr_2' => $kg5,'kg_val_3_for_hr_2' => $kg6
            ]);
        return $info;
    }
    public function update_khul_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6){
        $info = DB::table('delivery_based_settings')
            ->where('city','KHUL')
            ->update(['kg_val_1_for_hr_1' => $kg1,'kg_val_2_for_hr_1' => $kg2,'kg_val_3_for_hr_1' => $kg3,
                'kg_val_1_for_hr_2' => $kg4,'kg_val_2_for_hr_2' => $kg5,'kg_val_3_for_hr_2' => $kg6
            ]);
        return $info;
    }
    public function update_syl_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6){
        $info = DB::table('delivery_based_settings')
            ->where('city', 'SYL')
            ->update(['kg_val_1_for_hr_1' => $kg1,'kg_val_2_for_hr_1' => $kg2,'kg_val_3_for_hr_1' => $kg3,
                'kg_val_1_for_hr_2' => $kg4,'kg_val_2_for_hr_2' => $kg5,'kg_val_3_for_hr_2' => $kg6
            ]);
        return $info;
    }
    public function update_overall_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6){
        $info = DB::table('delivery_based_settings')
            ->where('city','Whole Bangladesh')
            ->update(['kg_val_1_for_hr_1' => $kg1,'kg_val_2_for_hr_1' => $kg2,'kg_val_3_for_hr_1' => $kg3,
                'kg_val_1_for_hr_2' => $kg4,'kg_val_2_for_hr_2' => $kg5,'kg_val_3_for_hr_2' => $kg6
            ]);
        return $info;
    }
    public function update_delivery_sms($sms){
        $info=DB::SELECT("SELECT * FROM delivery_sms WHERE id='1'");
        if(!$info){
            $info = DB::SELECT("INSERT INTO `delivery_sms` (`id`, `sms`) VALUES (1, '$sms')");
            return $info;
        }
        else{
            $info = DB::table('delivery_sms')
                ->where('id', '1')
                ->update(['sms' => $sms]);
            return $info;
        }
    }
    public function delivery_sms(){
        $info=DB::SELECT("SELECT * FROM delivery_sms WHERE id='1'");
        return $info;
    }


    


    
}
