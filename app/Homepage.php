<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;


class Homepage extends Model
{
    public function home_logo(){
        $info=DB::select("select `img` from site_img_n_logo where id='1'");
        return $info;
    }
    public function footer_logo(){
        $info=DB::select("select `img` from site_img_n_logo where id='3'");
        return $info;
    }
    public function person_1(){
        $info=DB::select("select `img` from chat_person where id='1'");
        return $info;
    }
    public function person_2(){
        $info=DB::select("select `img` from chat_person where id='2'");
        return $info;
    }
    public function person_3(){
        $info=DB::select("select `img` from chat_person where id='3'");
        return $info;
    }
    public function slider_image_1(){
        $info=DB::select("select `img` from site_img_n_logo where id='4'");
        return $info;
    }
    public function slider_image_2(){
        $info=DB::select("select `img` from site_img_n_logo where id='5'");
        return $info;
    }
    public function slider_image_3(){
        $info=DB::select("select `img` from site_img_n_logo where id='6'");
        return $info;
    }
    public function ride_image(){
        $info=DB::select("select `img`,`dialougue` from site_img_n_logo where id='9'");
        return $info;
    }
    public function delivery_image(){
        $info=DB::select("select `img`,`dialougue` from site_img_n_logo where id='8'");
        return $info;
    }
    public function cheepset_delivery_charge(){
        $info=DB::select("select `hour_1`,`hour_2`,`kg_1_for_hr_1`,`kg_2_for_hr_1`,`kg_3_for_hr_1`,`kg_val_1_for_hr_1`,`kg_val_2_for_hr_1`,`kg_val_3_for_hr_1`,`kg_val_1_for_hr_2`,`kg_val_2_for_hr_2`,`kg_val_3_for_hr_2` from delivery_based_settings where id='6'");
        return $info;
    }
    public function delivery_direction(){
        $info=DB::select("select * from delivery_sms ");
        return $info;
    }
    public function faq(){
        $info=DB::select("select * from fqa_qstn ");
        return $info;
    }
    public function add_passenger($name,$phone,$area,$gender)
    {
        if ($area == 'motijhil' || $area == 'mirpur' || $area == 'badda' || $area == 'uttara' || $area == 'banani' || $area == 'old_dhaka' || $area == 'mdpur' || $area == 'malibag' || $area == 'dhanmondi' || $area == 'jatrabari'){
            $location = 'DHK';
        }
        else{
            $location='CTG';
        }
        $query=DB::table('passenger')
            ->insert( ['passenger_name'=>$name,'passenger_phn'=>$phone,'location'=>$location,'area'=>$area,'gender'=>$gender] );
        return $query;
    }
    public function add_biker($name,$phone,$area,$gender)
    {
        if ($area == 'motijhil' || $area == 'mirpur' || $area == 'badda' || $area == 'uttara' || $area == 'banani' || $area == 'old_dhaka' || $area == 'mdpur' || $area == 'malibag' || $area == 'dhanmondi' || $area == 'jatrabari'){
            $location = 'DHK';
        }
        else{
            $location='CTG';
        }
        $query=DB::table('biker')
            ->insert( ['biker_name'=>$name,'biker_phn'=>$phone,'location'=>$location,'area'=>$area,'gender'=>$gender] );
        return $query;
    }
    public function check_ansr($id,$given_ansr){
        $correct_ansr=0;
//        $c=count($id);
//        echo '<pre>';
//        var_dump($given_ansr);die;
        $x=0;
        for($i=0;$i<count($id);$i++){
            $x++;
            $info[$i] = DB::table('fqa_qstn')
                ->select(DB::raw('correct_answer'))
                ->where('id', $id[$x])
                ->get();
        }

        $y=0;
        for($j=0;$j<count($info);$j++){
            $y++;
            foreach ($info[$j] as $ansr){
                foreach ($ansr as $a){
                    if($a==$given_ansr[$y]){
                        $correct_ansr+=1;
                    }
                }
            }
        }
//        echo '<pre>';
//        var_dump($correct_ansr);die;

        return $correct_ansr;
    }


}
