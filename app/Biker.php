<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;


class Biker extends Model
{
    public function biker_info()
    {
        $user = DB::table('biker')
            ->select(DB::raw('biker.biker_name, biker.id,biker_payment.earn,biker_payment.company_due,biker_payment.from_due,biker_payment.payment_date,biker_payment.user'))
            ->join('biker_payment', 'biker_payment.biker_id', '=', 'biker.id')
            ->orderBy('biker.id','ASC')
            ->get();
        return $user;
    }
    public function trip_type(){
        $info=DB::select('select `trip_type`, `biker_id`, count(*) as tp from ride group by `trip_type`, `biker_id`');
        return $info;
    }
    public function total_trip(){
        $info=DB::select('SELECT biker_id, COUNT(`trip_type`) as tp
        FROM ride GROUP BY `biker_id`');
        return $info;

    }
    public function biker_edit($id)
    {
        $biker = DB::table('biker')
            ->select(DB::raw('biker.id,biker.biker_name,biker_payment.earn,biker_payment.company_due,biker_payment.from_due,biker_payment.payment_date'))
            ->join('biker_payment', 'biker_payment.biker_id', '=', 'biker.id')
            ->where('biker.id', '=',$id)
            ->get();
        return $biker;
    }
    public function update_biker($id,$biker_name,$earn,$company_due,$from_due,$payment_date)
    {

        $info = DB::table('biker')
                ->where('biker.id', $id)
                ->update(['biker.biker_name' => $biker_name]);

        $info2 = DB::table('biker_payment')
            ->where('biker_payment.biker_id', $id)
            ->update(['biker_payment.earn' => $earn,'biker_payment.company_due' => $company_due,'biker_payment.from_due' => $from_due,'biker_payment.payment_date' => $payment_date]);
            return $info;

    }
    public function biker_daily_ride(){
        $info=DB::select('SELECT a.user,a.start_time,a.end_time,a.date,a.pick_up_area,a.destination_area,a.total_km,a.trip_type,a.raferal,a.biker_id,
b.biker_name,b.location,c.passenger_phn,d.earn
FROM ( SELECT ride.user, ride.start_time,ride.end_time,ride.date,ride.pick_up_area,ride.destination_area,ride.total_km,ride.trip_type,ride.raferal,ride.biker_id,ride.passenger_id 
FROM ride )a LEFT JOIN (SELECT biker.id,biker.biker_name,biker.location FROM biker)b ON a.biker_id=b.id 
LEFT JOIN(SELECT passenger.passenger_phn, passenger.id FROM passenger)c ON c.id=a.passenger_id
LEFT JOIN(SELECT biker_payment.biker_id,biker_payment.earn FROM biker_payment)d ON a.biker_id=d.biker_id');
        return $info;
    }
    public function biker_name($id){
        $info=DB::select("SELECT biker.biker_name FROM biker WHERE biker.id='.$id.'");
        return $info;
    }
    public function biker_details($id){
        $info=DB::select('SELECT a.ride_id, a.user,a.start_time,a.end_time,a.date,a.pick_up_area,a.destination_area,a.total_km,a.trip_type,a.raferal,a.biker_id,
b.biker_name,b.location,c.passenger_phn,d.earn
FROM ( SELECT ride.id ride_id, ride.user, ride.start_time,ride.end_time,ride.date,ride.pick_up_area,ride.destination_area,ride.total_km,ride.trip_type,ride.raferal,ride.biker_id,ride.passenger_id 
FROM ride WHERE ride.biker_id= '.$id.' )a LEFT JOIN (SELECT biker.id,biker.biker_name,biker.location FROM biker)b ON a.biker_id=b.id 
LEFT JOIN(SELECT passenger.passenger_phn, passenger.id FROM passenger)c ON c.id=a.passenger_id
LEFT JOIN(SELECT biker_payment.biker_id,biker_payment.earn FROM biker_payment)d ON a.biker_id=d.biker_id');
        return $info;
    }
    public function top_ride(){
        $query=DB::select('SELECT SUM(fp+mp) as tp,a.biker_id,a.fp,a.mp,a.end_time,a.date,a.fare,b.biker_name
 FROM( SELECT `biker_id`, SUM(`full_trip`) AS fp,SUM(`mis_trip`) AS mp,SUM(`fare`) AS fare,ride.end_time,ride.date 
 FROM ride GROUP BY `biker_id` )a LEFT JOIN(SELECT biker.id,biker.biker_name FROM biker)b
 ON b.id=a.biker_id GROUP BY a.biker_id ORDER BY tp,a.biker_id ');
        return $query;
    }
    public function lifetime_earning(){
        $query=DB::select('SELECT * FROM (SELECT ride.start_time,ride.end_time,ride.date,ride.pick_up_area,ride.destination_area,
ride.total_km,ride.trip_type,ride.rating,ride.fare,ride.raferal, ride.user,ride.passenger_id FROM ride)a 
LEFT JOIN (SELECT passenger.id,passenger.passenger_phn FROM passenger)b 
ON b.id=a.passenger_id ORDER BY a.start_time');
        return $query;
    }
    public function biker_profile($id){
        $query=DB::select("SELECT * FROM biker WHERE biker.id='$id'");
        return $query;
    }
    public function biker_profil_update($id,$bike_name,$biker_name,$phn,$nid,$reg,$gender,$location){
        $info = DB::table('biker')
            ->where('biker.id', $id)
            ->update(['bike_name' => $bike_name,'biker_phn' => $phn,'biker_name' => $biker_name,'biker_nid' => $nid,'biker_reg' => $reg,'gender' => $gender,'location' => $location]);
        return $info;
    }
    public function detail_delete($id)
    {
        $query=DB::table('ride')
            ->where('id','=',$id)
            ->delete();
        return $query;
    }
}
