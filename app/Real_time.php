<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;


class Real_time extends Model
{
    public function biker_on_dhaka()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE location='DHK'");
        return $user;
    }
    public function biker_on_motijhil()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE area='motijhil'");
        return $user;
    }
    public function biker_on_jatrabari()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE area='jatrabari'");
        return $user;
    }
    public function biker_on_old_dhaka()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE area='old_dhaka'");
        return $user;
    }
    public function biker_on_malibag()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE area='malibag'");
        return $user;
    }
    public function biker_on_badda()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE area='badda'");
        return $user;
    }
    public function biker_on_dhanmondi()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE area='dhanmondi'");
        return $user;
    }
    public function biker_on_mirpur()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE area='mirpur'");
        return $user;
    }
    public function biker_on_mdpur()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE area='mdpur'");
        return $user;
    }
    public function biker_on_banani()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE area='banani'");
        return $user;
    }
    public function biker_on_uttara()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE area='uttara'");
        return $user;
    }
    public function biker_on_ctg()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE location='chttg'");
        return $user;
    }
    public function passenger_on_dhaka()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE location='DHK'");
        return $user;
    }
    public function passenger_on_motijhil()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE area='motijhil'");
        return $user;
    }
    public function passenger_on_jatrabari()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE area='jatrabari'");
        return $user;
    }
    public function passenger_on_old_dhaka()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE area='old_dhaka'");
        return $user;
    }
    public function passenger_on_malibag()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE area='malibag'");
        return $user;
    }
    public function passenger_on_badda()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE area='badda'");
        return $user;
    }
    public function passenger_on_dhanmondi()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE area='dhanmondi'");
        return $user;
    }
    public function passenger_on_mirpur()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE area='mirpur'");
        return $user;
    }
    public function passenger_on_mdpur()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE area='mdpur'");
        return $user;
    }
    public function passenger_on_banani()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE area='banani'");
        return $user;
    }
    public function passenger_on_uttara()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE area='uttara'");
        return $user;
    }
    public function passenger_on_ctg()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM passenger WHERE location='CTG'");
        return $user;
    }
    public function biker_online_dhaka()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE active_online='1' AND location='DHK'");
        return $user;
    }
    public function biker_online_ctg()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE active_online='1' AND location='chttg'");
        return $user;
    }
    public function biker_onride_dhaka()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE on_ride='1' AND location='DHK'");
        return $user;
    }
    public function biker_onride_ctg()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE on_ride='1' AND location='chttg'");
        return $user;
    }
    public function biker_unverify_dhaka()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE unverify='0' AND location='DHK'");
        return $user;
    }
    public function biker_unverify_ctg()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE unverify='0' AND location='chttg'");
        return $user;
    }
    public function male_biker()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE gender='male' AND location='DHK'");
        return $user;
    }
    public function female_biker()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM biker WHERE gender='female' AND location='chttg'");
        return $user;
    }
    public function total_delivery()
    {
        $user = DB::SELECT("SELECT COUNT(*)as info FROM delivery WHERE on_the_way='0'");
        return $user;
    }
    public function on_way(){
        $user = DB::SELECT("SELECT COUNT(*)as info FROM delivery WHERE on_the_way='1'");
        return $user;
    }
    public function delivery_due_amount(){
        $user = DB::SELECT("SELECT id, SUM(company_due)as info FROM biker_payment GROUP BY id");
        return $user;
    }
    public function total_ride(){
        $user = DB::SELECT("SELECT COUNT(*)as info FROM ride ");
        return $user;
    }
    public function total_earn(){
        $user = DB::SELECT("SELECT id, SUM(earn)as info FROM biker_payment GROUP BY id");
        return $user;
    }
    public function total_company_earn($today){
        $user = DB::SELECT("SELECT id, SUM(company_due)as info FROM biker_payment WHERE from_due='$today' GROUP BY id");
        return $user;
    }
    public function total_company_refala($today){
        $user = DB::SELECT("SELECT id, SUM(raferal)as info FROM ride WHERE date='$today' GROUP BY id");
        return $user;
    }
    public function total_delivery_earn($today){
        $user = DB::SELECT("SELECT id, SUM(rcv_amount)as info FROM delivery WHERE date='$today' GROUP BY id");
        return $user;
    }
    public function new_delivery($today){
        $user = DB::SELECT("SELECT id, SUM(new_delivery)as info FROM delivery WHERE date='$today' GROUP BY id");
        return $user;
    }
    public function today_motijhil_order($today){
        $user = DB::SELECT("SELECT id, SUM(order_amount)as info FROM order_info WHERE date='$today' AND area='motijhil' GROUP BY id");
        return $user;
    }
    public function today_mirpur_order($today){
        $user = DB::SELECT("SELECT id, SUM(order_amount)as info FROM order_info WHERE date='$today' AND area='mirpur' GROUP BY id");
        return $user;
    }
    public function today_mdpur_order($today){
        $user = DB::SELECT("SELECT id, SUM(order_amount)as info FROM order_info WHERE date='$today' AND area='mdpur' GROUP BY id");
        return $user;
    }
    public function today_malibag_order($today){
        $user = DB::SELECT("SELECT id, SUM(order_amount)as info FROM order_info WHERE date='$today' AND area='malibag' GROUP BY id");
        return $user;
    }
    public function today_old_dhaka_order($today){
        $user = DB::SELECT("SELECT id, SUM(order_amount)as info FROM order_info WHERE date='$today' AND area='old_dhaka' GROUP BY id");
        return $user;
    }
    public function today_badda_order($today){
        $user = DB::SELECT("SELECT id, SUM(order_amount)as info FROM order_info WHERE date='$today' AND area='badda' GROUP BY id");
        return $user;
    }
}
