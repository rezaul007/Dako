<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;


class Stuff extends Model
{
    public function stuff_info()
    {
        $user = DB::SELECT('SELECT * FROM stuff');
        return $user;
    }
    public function check_stuff($stuff_name)
    {
        $stuff = DB::table('stuff')
            ->select(DB::raw('stuff_name'))
            ->where('stuff_name', '=',$stuff_name)
            ->get();
        return $stuff;
    }
    public function stuff_insert($today,$stuff_name,$permission,$stuff_type,$status,$phone,$password)
    {
        if ($status==2){
            $status=0;
        }
        else{
            $status=1;
        }
        $query=DB::table('stuff')
            ->insert( ['stuff_name'=>$stuff_name,'entry_date'=>$today,'stuff_type'=>$stuff_type,'permission'=>$permission,'stuff_phn'=>$phone,'password'=>$password] );

        if($query){
            return true;
        }
        else{
            return false;
        }
    }
    public function edit_stuff($id)
    {
        $stuff = DB::table('stuff')
            ->select(DB::raw('*'))
            ->where('id', '=',$id)
            ->get();
        return $stuff;
    }
    public function update_stuff($stuff_name,$permission,$stuff_type,$status,$phone,$id,$password,$today)
    {
        if ($status==2){
            $status=0;
        }
        else{
            $status=1;
        }
        if($password!=null) {

            $info = DB::table('stuff')
                ->where('id', $id)
                ->update(['stuff_name' => $stuff_name, 'stuff_type' => $stuff_type, 'stuff_phn' => $phone, 'permission' => $permission, 'status' => $status, 'modify_date' => $today, 'password' => $password]);

            return $info;
        }
        else{
            $info = DB::table('stuff')
                ->where('id', $id)
                ->update(['stuff_name' => $stuff_name, 'stuff_type' => $stuff_type, 'stuff_phn' => $phone, 'permission' => $permission, 'status' => $status, 'modify_date' => $today]);

            return $info;
        }
    }
    public function delete_stuff($id)
    {
        $stuff=DB::table('stuff')
            ->where('id','=',$id)
            ->delete();
        return $stuff;
    }
}
