<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;


class Passenger extends Model
{
    public function passenger_info()
    {
        $user = DB::table('passenger')
            ->select(DB::raw('passenger.passenger_name, passenger.id,biker.biker_phn,
            ride.start_time,ride.end_time,ride.date,ride.user,ride.pick_up_area,ride.destination_area,ride.total_km,
            ride.trip_type,ride.raferal'))
            ->join('ride', 'ride.passenger_id', '=', 'passenger.id')
            ->join('biker', 'ride.biker_id', '=', 'biker.id')
            ->get();
        return $user;
    }
    public function lifetime_raiding(){
        $query=DB::select('SELECT * FROM (SELECT ride.start_time,ride.end_time,ride.date,ride.pick_up_area,ride.destination_area,
ride.total_km,ride.trip_type,ride.rating,ride.fare,ride.raferal, ride.user,ride.biker_id FROM ride)a 
LEFT JOIN (SELECT biker.id,biker.biker_phn FROM biker)b 
ON b.id=a.biker_id ORDER BY a.start_time');
        return $query;
    }
    public function passenger_detail($id){
        $info=DB::select('SELECT a.ride_id, a.user,a.start_time,a.end_time,a.date,a.pick_up_area,a.destination_area,a.total_km,a.trip_type,a.raferal,a.passenger_id,
b.biker_phn,c.passenger_phn,c.passenger_name
FROM ( SELECT ride.id ride_id, ride.user, ride.start_time,ride.end_time,ride.date,ride.pick_up_area,ride.destination_area,ride.total_km,ride.trip_type,ride.raferal,ride.biker_id,ride.passenger_id 
FROM ride WHERE ride.passenger_id= '.$id.' )a LEFT JOIN (SELECT biker.id,biker.biker_phn FROM biker)b ON a.biker_id=b.id 
LEFT JOIN(SELECT passenger.passenger_phn, passenger.id,passenger.passenger_name FROM passenger)c ON c.id=a.passenger_id');
        return $info;
    }
    public function passenger_name($id){
        $info=DB::select("SELECT passenger.passenger_name FROM passenger WHERE passenger.id='.$id.'");
        return $info;
    }
    public function passenger_profile($id){
        $info=DB::select("SELECT * FROM passenger WHERE passenger.id='.$id.'");
        return $info;
    }
    public function passenger_profil_update($id,$name,$phn,$gender){
        $info = DB::table('passenger')
            ->where('passenger.id', $id)
            ->update(['passenger_name' => $name,'passenger_phn' => $phn,'gender' => $gender]);
        return $info;
    }
    public function detail_delete($id)
    {
        $query=DB::table('ride')
            ->where('id','=',$id)
            ->delete();
        return $query;
    }
}
