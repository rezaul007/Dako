<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Real_time;
use Illuminate\Support\Facades\Redirect;

class Real_timeController extends Controller
{
    public function __construct(Request $request)
    {
        $user_id=$request->session()->get('user_id');
        if($user_id==null){
            $error_msg=null;
            Redirect::to('/')->send();
        }
    }
    public function index()
    {
        $real_time_model = new Real_time();
        $biker_on_dhaka=$real_time_model->biker_on_dhaka();
        $biker_online_dhaka=$real_time_model->biker_online_dhaka();
        $biker_online_ctg=$real_time_model->biker_online_ctg();
        $biker_onride_dhaka=$real_time_model->biker_onride_dhaka();
        $biker_onride_ctg=$real_time_model->biker_onride_ctg();
        $biker_unverify_dhaka=$real_time_model->biker_unverify_dhaka();
        $biker_unverify_ctg=$real_time_model->biker_unverify_ctg();
        $male_biker=$real_time_model->male_biker();
        $female_biker=$real_time_model->female_biker();


        $biker_on_jatrabari=$real_time_model->biker_on_jatrabari();
        $biker_on_motijhil=$real_time_model->biker_on_motijhil();
        $biker_on_old_dhaka=$real_time_model->biker_on_old_dhaka();
        $biker_on_malibag=$real_time_model->biker_on_malibag();
        $biker_on_badda=$real_time_model->biker_on_badda();
        $biker_on_dhanmondi=$real_time_model->biker_on_dhanmondi();
        $biker_on_mirpur=$real_time_model->biker_on_mirpur();
        $biker_on_mdpur=$real_time_model->biker_on_mdpur();
        $biker_on_banani=$real_time_model->biker_on_banani();
        $biker_on_uttara=$real_time_model->biker_on_uttara();

        $biker_on_ctg=$real_time_model->biker_on_ctg();
        $passenger_on_dhaka=$real_time_model->passenger_on_dhaka();

        $passenger_on_jatrabari=$real_time_model->passenger_on_jatrabari();
        $passenger_on_motijhil=$real_time_model->passenger_on_motijhil();
        $passenger_on_old_dhaka=$real_time_model->passenger_on_old_dhaka();
        $passenger_on_malibag=$real_time_model->passenger_on_malibag();
        $passenger_on_badda=$real_time_model->passenger_on_badda();
        $passenger_on_dhanmondi=$real_time_model->passenger_on_dhanmondi();
        $passenger_on_mirpur=$real_time_model->passenger_on_mirpur();
        $passenger_on_mdpur=$real_time_model->passenger_on_mdpur();
        $passenger_on_banani=$real_time_model->passenger_on_banani();
        $passenger_on_uttara=$real_time_model->passenger_on_uttara();

        $passenger_on_ctg=$real_time_model->passenger_on_ctg();
        $total_delivery=$real_time_model->total_delivery();
        $delivery_due_amount=$real_time_model->delivery_due_amount();
        $on_way=$real_time_model->on_way();

        $today = date('Y-m-d');
        $total_ride=$real_time_model->total_ride();
        $total_earn=$real_time_model->total_earn();
        $total_company_earn=$real_time_model->total_company_earn($today);
        $total_company_refala=$real_time_model->total_company_refala($today);
        $total_delivery_earn=$real_time_model->total_delivery_earn($today);
        $new_delivery=$real_time_model->new_delivery($today);
        $today_motijhil_order=$real_time_model->today_motijhil_order($today);
        $today_mirpur_order=$real_time_model->today_mirpur_order($today);
        $today_mdpur_order=$real_time_model->today_mdpur_order($today);
        $today_malibag_order=$real_time_model->today_malibag_order($today);
        $today_old_dhaka_order=$real_time_model->today_old_dhaka_order($today);
        $today_badda_order=$real_time_model->today_badda_order($today);
        
//        echo '<pre>';
//        var_dump($biker_info);die;
        return view('real_time/real_time')->with(compact('biker_on_dhaka','biker_on_ctg','passenger_on_dhaka','passenger_on_ctg','biker_on_motijhil',
        'biker_on_jatrabari','biker_on_old_dhaka','biker_on_malibag','biker_on_badda','biker_on_dhanmondi','biker_on_mirpur','biker_on_mdpur',
            'biker_on_banani','biker_on_uttara','passenger_on_jatrabari','passenger_on_motijhil','passenger_on_old_dhaka','passenger_on_malibag',
            'passenger_on_badda','passenger_on_dhanmondi','passenger_on_mirpur','passenger_on_mdpur','passenger_on_banani','passenger_on_uttara',
            'biker_online_dhaka','biker_online_ctg','biker_onride_dhaka','biker_onride_ctg','biker_unverify_dhaka','biker_unverify_ctg','male_biker','female_biker',
            'total_delivery','on_way','delivery_due_amount','total_earn','total_company_refala','total_company_earn','total_ride','total_delivery_earn',
            'new_delivery','today_motijhil_order','today_mirpur_order','today_mdpur_order','today_malibag_order','today_old_dhaka_order','today_badda_order'

        ));
    }

}
