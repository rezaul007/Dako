<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Settings;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class SettingsController extends Controller
{
    public function __construct(Request $request)
    {
        $user_id=$request->session()->get('user_id');
        if($user_id==null){
            $error_msg=null;
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request)
    {
        $info=$request->session()->get('info');
        if ($info!=null){
            $person_info=$info;
        }
        else{
            $person_info=null;
        }
        $settings = new Settings();
        $sms_info=$settings->sms_api();
        $area_info=$settings->tracking_area();
        $pick_time_info=$settings->pick_time_info();
        $ride_request=$settings->ride_request();
        $refaral_how_many=$settings->refaral_how_many();
        $request_count_stage=$settings->request_count_stage();
        $account_info=$settings->account_info();
        $whole_site_info=$settings->whole_site_info();
        $site_img_n_logo=$settings->site_img_n_logo();
        $dhaka_hours=$settings->dhaka_hours();
        $dhaka_kg=$settings->dhaka_kg();
        $dhaka_val=$settings->dhaka_val();
        $count_city=$settings->count_city();
        $other_city_hours=$settings->other_city_hours();
        $other_city_kg=$settings->other_city_kg();
        $ctg_city_val=$settings->ctg_city_val();        
        $raj_city_val=$settings->raj_city_val();
        $khul_city_val=$settings->khul_city_val();
        $syl_city_val=$settings->syl_city_val();
        $overall_city_val=$settings->overall_city_val();        
        $delivery_sms=$settings->delivery_sms();        
//        echo '<pre>';
//        for($s=0;$s<count($other_city_val);$s++){
//            echo $other_city_val[0];
//            die;
//        }
//        foreach($other_city_val as $hr){
//            var_dump($hr->city);}
//            die;
        return view('settings/settings')->with(compact('sms_info','area_info','pick_time_info','ride_request','refaral_how_many','request_count_stage','account_info','person_info','whole_site_info','site_img_n_logo','dhaka_hours','dhaka_kg','dhaka_val','count_city','other_city_hours','other_city_kg','ctg_city_val','raj_city_val','khul_city_val','syl_city_val','overall_city_val','delivery_sms'));
    }
    public function update_sms_api()
    {
        $settings_model = new Settings();
        
        $id = $_POST['id'];
        $total_sms = $_POST['total_sms'];
        $unused_sms = $_POST['unused_sms'];
        $sms_info=$settings_model->update_sms($id,$total_sms,$unused_sms);
        return redirect('settings');
    }
    public function update_tracking_area()
    {
        $settings_model = new Settings();

        $id = $_POST['id'];
        $area = $_POST['area'];
        $sms_info=$settings_model->update_tracking_area($id,$area);
        return redirect('settings');
    }
    public function update_pick_up_schedule()
    {
        for($i=1;$i<6;$i++){
            $id[$i]=$_POST['id/'.$i];
        }
        for($i=1;$i<6;$i++){
            $schedule[$i]=$_POST[$i.'/schedule'];
        }
//        echo '<pre>';
//        var_dump($schedule[1]);die;
        $settings_model = new Settings();

//        $id = $_POST['id'];
//        $schedule = $_POST['schedule'];
        $sms_info=$settings_model->update_schedule($id,$schedule);
        return redirect('settings');
    }
    public function update_pick_up_tk()
    {
        for($i=1;$i<6;$i++){
            $id[$i]=$_POST['id/'.$i];
        }
        for($i=1;$i<6;$i++){
            $tk[$i]=$_POST[$i.'/tk'];
        }
//        $s = "Lesson 001: Complete";
//        echo '<pre>';
//        var_dump($tk[1]);
//        echo filter_var($tk[1], FILTER_SANITIZE_NUMBER_INT);
//
//        die;
        $settings_model = new Settings();
        if(array_key_exists('average_time_status',$_POST )){
            $average_time_status = $_POST['average_time_status'];
        }
        else{
            $average_time_status = null;
        }

        $per_min_rate = $_POST['per_min_rate'];
//        var_dump($per_min_rate);die;
        $sms_info=$settings_model->update_tk($id,$tk,$per_min_rate,$average_time_status);
        return redirect('settings');
    }
    public function update_pick_up_average_tk()
    {
//        echo '<pre>';
//        var_dump($_POST);die;
//        $s = "Lesson 001: Complete";

        if(array_key_exists('average_time_status',$_POST )){
            $average_time_status = $_POST['average_time_status'];
        }
        else{
            $average_time_status = null;
        }
        $settings_model = new Settings();
        $id = $_POST['id'];
        $average_tk = $_POST['average_tk'];
//        echo '<pre>';
//        var_dump($average_tk);
//        echo filter_var($average_tk, FILTER_SANITIZE_NUMBER_INT);
//
//        die;
        $per_min_rate = $_POST['per_min_rate'];
        $sms_info=$settings_model->update_average_tk($average_tk,$per_min_rate,$average_time_status);
        return redirect('settings');
    }
    public function update_ride_request(){
        for($i=1;$i<3;$i++){
            $id[$i]=$_POST['id/'.$i];
        }
        for($i=1;$i<3;$i++){
            $area[$i]=$_POST['area/'.$i];
        }
        for($i=1;$i<3;$i++){
            $time[$i]=$_POST['time/'.$i];
        }
//        echo '<pre>';
////        var_dump($id);
////        echo '<br>';
//        var_dump($time);
//        die;
        $settings_model = new Settings();
        $sms_info=$settings_model->update_ride_request($id,$area,$time);
        return redirect('settings');
    }
    public function update_non_average_refaral(){
        if(array_key_exists('non_average_refaral/1',$_POST )){
            $average_status[1] =0;
        }
        else{
            $average_status[1] = 1;
        }
        if(array_key_exists('non_average_refaral/2',$_POST )){
            $average_status[2] =0;
        }
        else{
            $average_status[2] = 1;
        }
        for ($i=1;$i<3;$i++){
            $id[$i] = $_POST['id/'.$i];
            $first[$i] = $_POST['first/'.$i];
            $after[$i] = $_POST['after/'.$i];
        }
        $settings_model = new Settings();
        $sms_info=$settings_model->update_non_average_refaral($id,$first,$after,$average_status);
        return redirect('settings');
    }
    public function update_average_refaral(){
//        echo '<pre>';
//        var_dump($_POST);
//        die;
        if(array_key_exists('average_refaral/1',$_POST )){
            $average_status[1] =1;
        }
        else{
            $average_status[1] = 0;
        }
        if(array_key_exists('average_refaral/2',$_POST )){
            $average_status[2] =1;
        }
        else{
            $average_status[2] = 0;
        }
        for ($i=1;$i<3;$i++){
            $id[$i] = $_POST['id/'.$i];
            $average[$i] = $_POST['average/'.$i];
        }
        $settings_model = new Settings();
        $sms_info=$settings_model->update_average_refaral($id,$average,$average_status);
        return redirect('settings');
    }
    public function update_request_stage(){
//        echo '<pre>';
//        var_dump($_POST);
//        die;
        $id = $_POST['status'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_request_stage($id);
        return redirect('settings');

    }
    public function update_account_info(){
        for ($i=1;$i<4;$i++){
            $id[$i] = $_POST['id/'.$i];
            $account_name[$i] = $_POST['account_name/'.$i];
            $account_no[$i] = $_POST['account_no/'.$i];
        }
        $settings_model = new Settings();
        $stage_info=$settings_model->update_account_info($id,$account_name,$account_no);
        return redirect('settings');
    }
    public function search_block_person(){
        $info = $_POST['info'];
        $settings_model = new Settings();
        $person_info=$settings_model->search_person($info);
        if($person_info!=null){
        foreach ($person_info as $person){
//            echo '<pre>';
//            var_dump($person);die;
            if(array_key_exists('info', $person)){
                $i=$person->info;
                $info=session()->put('info', $i);
            }
            elseif (array_key_exists('info1', $person) && $person->info1!=null){
                $i=$person->info1;
                $info=session()->put('info', $i);
            }
            elseif(array_key_exists('info2', $person) && $person->info2!=null){
                $i=$person->info2;
                $info=session()->put('info', $i);
            }

        }
        }
        else{
            $i='Not found';
            $info=session()->put('info', $i);
        }
        return redirect('settings');
    }
    public function block_person(){
        $info = $_POST['info'];
        $settings_model = new Settings();
        $person_info=$settings_model->block_person($info);
        return redirect('settings');
    }
    public function insert_faq(){
//        echo '<pre>';
//        var_dump($_POST);die;

        $qstn = $_POST['qstn'];
        for($i=1;$i<4;$i++){
            if(array_key_exists('right/'.$i,$_POST)){
                $correct_ansr = $_POST['right/'.$i];
            }
        }
//        echo '<pre>';
//        var_dump($correct_ansr);
//        die;
        $ansr1 = $_POST['ansr1'];
        $ansr2 = $_POST['ansr2'];
        $ansr3 = $_POST['ansr3'];
        $settings_model = new Settings();
        $insert_info=$settings_model->insert_faq($qstn,$correct_ansr,$ansr1,$ansr2,$ansr3);
        return redirect('settings');
    }
    public function update_whole_site_settings(){

            $id = $_POST['id'];
            $position = $_POST['position'];
            $link = $_POST['link'];
        if($_FILES['img']['name']!=null){
            $filename = $_FILES['img']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $file_name = $id . '.' . $ext;
            $img = $file_name;
        }
        else{
            $settings_model = new Settings();
            $info=$settings_model->check_img($id);
            if($info!=null){
                foreach ($info as $im){
                    $img = $im->img;
                }
            }
            else{
                $img=null;
            }
        }
        if(array_key_exists('position_active',$_POST )){
            $posiotion_active=1;
        }
        else{
            $posiotion_active=0;
        }
        if($_FILES['img']['name']!=null) {
            $filename = $_FILES['img']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $file_name = $id . '.' . $ext;
//            var_dump($file_name);die;
            $path = public_path("uploaded_image/whole_site_n_app_based_image");
            $uploadSuccess = Input::file('img')->move($path, $file_name);
            if ($uploadSuccess == null) {
                session()->put('error_msg', 'Image upload unsuccessful..');
            }
        }
        $settings_model = new Settings();
        $stage_info=$settings_model->update_whole_site_settings($id,$posiotion_active,$position,$img,$link);
        return redirect('settings');
    }
    public function update_site_image_n_logo(){
//        echo '<pre>';
//        var_dump($_POST);
//        die;
        $id = $_POST['id'];
        $dialougue = $_POST['dialougue'];
        
        if($_FILES['img']['name']!=null){
                $filename = $_FILES['img']['name'];
                $info = pathinfo(storage_path() . $filename);
                $ext = $info['extension'];
                $file_name = $id . '.' . $ext;
                $img = $file_name;

        }
        else{
            $settings_model = new Settings();
            $info=$settings_model->check_site_n_logo_img($id);
            if($info!=null){
                foreach ($info as $im){
                    $img = $im->img;
                }
            }
            else{
                $img=null;
            }
        }
        if($_FILES['img']['name']!=null) {
                $filename = $_FILES['img']['name'];
                $info = pathinfo(storage_path() . $filename);
                $ext = $info['extension'];
                $file_name = $id . '.' . $ext;
                $path = public_path("uploaded_image/site_img_n_logo");
                $uploadSuccess = Input::file('img')->move($path, $file_name);
                if ($uploadSuccess == null) {
                    session()->put('error_msg', 'Image upload unsuccessful..');
                }
            
        }
        $settings_model = new Settings();
        $stage_info=$settings_model->update_site_image_n_logo($id,$dialougue,$img);
        return redirect('settings');
    }
    public function update_auto_chat(){
//        echo '<pre>';
//        var_dump($_POST);
//        die;
        $msg = $_POST['msg'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_auto_chat($msg);
        return redirect('settings');
    }
    public function update_chat_person(){

        if($_FILES['img_1']['name']!=null){
            $filename = $_FILES['img_1']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $n='person_1';
            $file_name= $n . '.' . $ext;
            $img_1 = $file_name;
        }
        else{
            $name='person_1';
            $settings_model = new Settings();
            $info=$settings_model->person_img_check($name);
            if($info!=null){
                foreach ($info as $im){
                    $img = $im->img;
                }
            }
            else{
                $img=null;
            }
        }
        if($_FILES['img_1']['name']!=null) {
            $filename = $_FILES['img_1']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $n='person_1';
            $file_name_1= $n. '.' . $ext;
//            var_dump($file_name_1);
//            die;
            $path = public_path("uploaded_image/chat_persons");
            $uploadSuccess = Input::file('img_1')->move($path, $file_name_1);

            if ($uploadSuccess == null) {
                session()->put('error_msg', 'Image upload unsuccessful..');
            }
        }
        if($_FILES['img_2']['name']!=null){
            $filename = $_FILES['img_2']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $nn='person_2';
            $file_name = $nn . '.' . $ext;
            $img_2 = $file_name;
        }
        else{
            $name='person_2';
            $settings_model = new Settings();
            $info=$settings_model->person_img_check($name);
            if($info!=null){
                foreach ($info as $im){
                    $img = $im->img;
                }
            }
            else{
                $img=null;
            }
        }
        if($_FILES['img_2']['name']!=null) {
            $filename = $_FILES['img_2']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $nn='person_2';
            $file_name_2= $nn . '.' . $ext;
            $path = public_path("uploaded_image/chat_persons");
            $uploadSuccess = Input::file('img_2')->move($path, $file_name_2);
            if ($uploadSuccess == null) {
                session()->put('error_msg', 'Image upload unsuccessful..');
            }
        }
        if($_FILES['img_3']['name']!=null){
            $filename = $_FILES['img_3']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $nn='person_3';
            $file_name= $nn . '.' . $ext;
            $img_3 = $file_name;
        }
        else{
            $name='person_3';
            $settings_model = new Settings();
            $info=$settings_model->person_img_check($name);
            if($info!=null){
                foreach ($info as $im){
                    $img = $im->img;
                }
            }
            else{
                $img=null;
            }
        }
        if($_FILES['img_3']['name']!=null) {
            $filename = $_FILES['img_3']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $nnn='person_3';
            $file_name_3= $nnn . '.' . $ext;
            $path = public_path("uploaded_image/chat_persons");
            $uploadSuccess = Input::file('img_3')->move($path, $file_name_3);
            if ($uploadSuccess == null) {
                session()->put('error_msg', 'Image upload unsuccessful..');
            }
        }

        $settings_model = new Settings();
        $stage_info=$settings_model->update_chat_person($img_1,$img_2,$img_3);
        return redirect('settings');
    }
    public function update_dhaka_hours(){
        $hr1 = $_POST['hr1'];
        $hr2 = $_POST['hr2'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_dhaka_hours($hr1,$hr2);
        return redirect('settings');
    }
    public function update_dhaka_kg(){
        $kg1 = $_POST['half_kg_1'];
        $kg2 = $_POST['1_kg_1'];
        $kg3 = $_POST['2_kg_1'];
        $kg4 = $_POST['half_kg_2'];
        $kg5 = $_POST['1_kg_2'];
        $kg6 = $_POST['2_kg_2'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_dhaka_kg($kg1,$kg2,$kg3,$kg4,$kg5,$kg6);
        return redirect('settings');
    }
    public function update_dhaka_kg_val(){
        $kg1 = $_POST['24_val_1'];
        $kg2 = $_POST['24_val_2'];
        $kg3 = $_POST['24_val_3'];
        $kg4 = $_POST['48_val_1'];
        $kg5 = $_POST['48_val_2'];
        $kg6 = $_POST['48_val_3'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_dhaka_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6);
        return redirect('settings');
    }
    public function update_other_hours(){
        $hr1 = $_POST['hr1'];
        $hr2 = $_POST['hr2'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_other_hours($hr1,$hr2);
        return redirect('settings');
    }
    public function update_other_kg(){
        $kg1 = $_POST['half_kg_1'];
        $kg2 = $_POST['1_kg_1'];
        $kg3 = $_POST['2_kg_1'];
        $kg4 = $_POST['half_kg_2'];
        $kg5 = $_POST['1_kg_2'];
        $kg6 = $_POST['2_kg_2'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_other_kg($kg1,$kg2,$kg3,$kg4,$kg5,$kg6);
        return redirect('settings');
    }
    public function update_ctg_kg_val(){
        $kg1 = $_POST['24_val_1'];
        $kg2 = $_POST['24_val_2'];
        $kg3 = $_POST['24_val_3'];
        $kg4 = $_POST['48_val_1'];
        $kg5 = $_POST['48_val_2'];
        $kg6 = $_POST['48_val_3'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_ctg_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6);
        return redirect('settings');
    }
    public function update_raj_kg_val(){
        $kg1 = $_POST['24_val_1'];
        $kg2 = $_POST['24_val_2'];
        $kg3 = $_POST['24_val_3'];
        $kg4 = $_POST['48_val_1'];
        $kg5 = $_POST['48_val_2'];
        $kg6 = $_POST['48_val_3'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_raj_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6);
        return redirect('settings');
    }
    public function update_khul_kg_val(){
        $kg1 = $_POST['24_val_1'];
        $kg2 = $_POST['24_val_2'];
        $kg3 = $_POST['24_val_3'];
        $kg4 = $_POST['48_val_1'];
        $kg5 = $_POST['48_val_2'];
        $kg6 = $_POST['48_val_3'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_khul_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6);
        return redirect('settings');
    }
    public function update_syl_kg_val(){
        $kg1 = $_POST['24_val_1'];
        $kg2 = $_POST['24_val_2'];
        $kg3 = $_POST['24_val_3'];
        $kg4 = $_POST['48_val_1'];
        $kg5 = $_POST['48_val_2'];
        $kg6 = $_POST['48_val_3'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_syl_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6);
        return redirect('settings');
    }
    public function update_overall_kg_val(){
        $kg1 = $_POST['24_val_1'];
        $kg2 = $_POST['24_val_2'];
        $kg3 = $_POST['24_val_3'];
        $kg4 = $_POST['48_val_1'];
        $kg5 = $_POST['48_val_2'];
        $kg6 = $_POST['48_val_3'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_overall_kg_val($kg1,$kg2,$kg3,$kg4,$kg5,$kg6);
        return redirect('settings');
    }
    public function update_direction_sms(){
//        var_dump($_POST);
//        die;
        $sms = $_POST['delivery_sms'];
        $settings_model = new Settings();
        $stage_info=$settings_model->update_delivery_sms($sms);
        return redirect('settings');
        
    }
    
}
