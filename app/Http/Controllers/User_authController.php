<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Carbon\Carbon;
use Auth;

//use Illuminate\Support\Facades\Cache;

class User_authController extends Controller
{
    public function index(Request $request)
    {
        $email = $request->input('email');
        $password = md5($request->input('pass'));
        $users = DB::table('user')
            ->select(DB::raw('*'))
            ->where('email', '=',$email)
            ->where('password', '=',$password)
            ->where('active', '=', 1)
            ->get();
        if($users!=null){
            foreach($users as $user){
                $id= $user->id;
            }
            date_default_timezone_set('Asia/Dhaka');
            $date = Carbon::now();
            $date->toDateTimeString('Y-m-d h:i:s');
            $last_login = DB::table('user')
                ->where('id', $id)
                ->update(['last_login' => $date]);
            $request->session()->put('user_id', $id);
            
            foreach($users as $user){
                $name=$user->name;
                $image=$user->img;
                $request->session()->put('name', $name);//user name n image for header n menu
                $request->session()->put('image', $image);
            }
            return redirect('biker');
        }
        else{
            $request->session()->put('error_msg', 'Invalid user name or password');
            $error_msg=$request->session()->get('error_msg');
            return redirect('/')->with(compact('error_msg'));
        }
    }

    public function logout(Request $request)
    {
       $res= $request->session()->put('user_id', null);
        Auth::logout();
        $error_msg=null;
        return redirect('/')->with(compact('error_msg'));
    }
}
