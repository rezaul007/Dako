<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Unverify;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class UnverifyController extends Controller
{
    public function __construct(Request $request)
    {
        $user_id=$request->session()->get('user_id');
        if($user_id==null){
            $error_msg=null;
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request)
    {
        $unverify_model = new Unverify();
        $verify_info=$unverify_model->verify_info();
//        echo '<pre>';
//        var_dump($verify_info);die;
//        echo '<br>';
//        echo 'this is trip';
//        echo '<br>';
//        var_dump($trip);die;
        return view('unverify/unverify')->with(compact('verify_info'));
    }
    public function unverify_edit(Request $request,$id){
//        echo '<pre>';
//        echo "not working";
//        var_dump($_POST);die;
        $unverify_model = new Unverify();
        $biker_info=$unverify_model->edit_unverify($id);
        $id=$id;
        return view('unverify/edit_unverify')->with(compact('biker_info','id'));

    }
    public function update(Request $request){
//        echo '<pre>';
//        var_dump($_FILES);die;
//        date_default_timezone_set('Asia/Dhaka');
//        $today = Carbon::now();
//        $today->toDateTimeString('Y-m-d h:i:s');
        $id = $_POST['id'];
        $biker_name = $_POST['biker_name'];
        $phone = $_POST['phone'];
        $area = $_POST['area'];
        $gender = $_POST['gender'];
        if($_POST['nid']!=null){
            $nid = $_POST['nid'];
        }
        else{
            $nid = null;
        }
        if($_POST['reg']!=null){
            $reg = $_POST['reg'];
        }
        else{
            $reg = null;
        }
        if($_FILES['img']['name']!=null){
            $filename = $_FILES['img']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $file_name = $id . '.' . $ext;
            $img = $file_name;
        }
        else{
            $img = null;
        }


        $location = $_POST['location'];

        $model = $_POST['model'];
        $call_biker = $_POST['call_biker'];
        $call_from = $_POST['call_from'];
        $test_result = $_POST['test_result'];

        if($_FILES['img']['name']!=null) {
            $filename = $_FILES['img']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $file_name = $id . '.' . $ext;
//            var_dump($file_name);die;
            $path = public_path("uploaded_image/user_profile");
            $uploadSuccess = Input::file('img')->move($path, $file_name);
            if ($uploadSuccess == null) {
                $request->session()->put('error_msg', 'Image upload unsuccessful..');
            }
        }


        $unverify_model = new Unverify();
        $biker_info=$unverify_model->update_unverify($biker_name,$phone,$area,$gender,$nid,$reg,$location,$img,$model,$call_biker,$call_from,$test_result,$id);
//        echo '<pre>';
//        var_dump($stuff_info);die;
        if($biker_info){
            $msg='Updated successfully.';
            $error_msg=null;
            return redirect('unverify')->with(compact('error_msg','msg'));
        }
        else{
            $error_msg='Update unsuccessed';
            $msg=null;
            return redirect('unverify')->with(compact('error_msg','msg'));
        }

    }
    public function delete(Request $request,$id){
//        echo '<pre>';
//        var_dump($_POST);die;
        $unverify_model = new Unverify();

        $stuff_info=$unverify_model->delete_unverify($id);
//        echo '<pre>';
//        var_dump($stuff_info);die;
        return redirect('unverify');
    }
}
