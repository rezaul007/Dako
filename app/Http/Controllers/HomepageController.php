<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Homepage;
use Illuminate\Support\Facades\Redirect;

class HomepageController extends Controller
{
    public function index(){
        $home_model=new Homepage();
        $home_logo=$home_model->home_logo();
        if($home_logo){
            foreach ($home_logo as $logo){
                $home_logo=$logo->img;
            }
        }
        else{
            $home_logo='1.png';
        }
        $footer_logo=$home_model->footer_logo();
        if($footer_logo){
            foreach ($footer_logo as $logo){
                $footer_logo=$logo->img;
            }
        }
        else{
            $footer_logo='3.png';
        }
        $person_1=$home_model->person_1();
        if($person_1){
            foreach ($person_1 as $logo){
                $person_1=$logo->img;
            }
        }
        else{
            $person_1='person_1.png';
        }
        $person_2=$home_model->person_2();
        if($person_2){
            foreach ($person_2 as $logo){
                $person_2=$logo->img;
            }
        }
        else{
            $person_2='person_2.jpg';
        }
        $person_3=$home_model->person_3();
        if($person_3){
            foreach ($person_3 as $logo){
                $person_3=$logo->img;
            }
        }
        else{
            $person_3='person_3.jpg';
        }
        $slider_image_1=$home_model->slider_image_1();
        if($slider_image_1){
            foreach ($slider_image_1 as $logo){
                $slider_image_1=$logo->img;
            }
        }
        else{
            $slider_image_1='4.jpg';
        }
        $slider_image_2=$home_model->slider_image_2();
        if($slider_image_2){
            foreach ($slider_image_2 as $logo){
                $slider_image_2=$logo->img;
            }
        }
        else{
            $slider_image_2='5.jpg';
        }
        $slider_image_3=$home_model->slider_image_3();
        if($slider_image_3){
            foreach ($slider_image_3 as $logo){
                $slider_image_3=$logo->img;
            }
        }
        else{
            $slider_image_3='6.jpg';
        }

//        var_dump($slider_image_1);die;
        return view('home_pages/index')->with(compact('home_logo','footer_logo','person_1','person_2','person_3','slider_image_1','slider_image_2','slider_image_3'));
    }
    public function add_passenger()
    {
        $name = $_POST['name'];
        $phone = $_POST['phone'];
        $gender = $_POST['gender'];
        $area = $_POST['area'];
        $home_model = new Homepage();
        $passenger_info=$home_model->add_passenger($name,$phone,$area,$gender);
        return redirect('/');
    }
    public function add_biker()
    {
        $name = $_POST['name'];
        $phone = $_POST['phone'];
        $gender = $_POST['gender'];
        $area = $_POST['area'];
        $home_model = new Homepage();
        $passenger_info=$home_model->add_biker($name,$phone,$area,$gender);
        return redirect('become_rider');
    }
    public function delivery_page(){
        $home_model=new Homepage();
        $home_logo=$home_model->home_logo();
        if($home_logo){
            foreach ($home_logo as $logo){
                $home_logo=$logo->img;
            }
        }
        else{
            $home_logo='1.png';
        }
        $footer_logo=$home_model->footer_logo();
        if($footer_logo){
            foreach ($footer_logo as $logo){
                $footer_logo=$logo->img;
            }
        }
        else{
            $footer_logo='3.png';
        }
        $person_1=$home_model->person_1();
        if($person_1){
            foreach ($person_1 as $logo){
                $person_1=$logo->img;
            }
        }
        else{
            $person_1='person_1.png';
        }
        $person_2=$home_model->person_2();
        if($person_2){
            foreach ($person_2 as $logo){
                $person_2=$logo->img;
            }
        }
        else{
            $person_2='person_2.jpg';
        }
        $person_3=$home_model->person_3();
        if($person_3){
            foreach ($person_3 as $logo){
                $person_3=$logo->img;
            }
        }
        else{
            $person_3='person_3.jpg';
        }
        $home_model=new Homepage();
        $delivery_image=$home_model->delivery_image();
        if($delivery_image){
            foreach ($delivery_image as $logo){
                $delivery_image=$logo->img;
                $sms=$logo->dialougue;
            }
        }
        else{
            $delivery_image='8.jpg';
            $sms=null;
        }
        $home_model=new Homepage();
        $cheepset_delivery_charge=$home_model->cheepset_delivery_charge();
        foreach ($cheepset_delivery_charge as $charges){
            $hr1=$charges->hour_1;
            $hr2=$charges->hour_2;
            $kg1=$charges->kg_1_for_hr_1;
            $kg2=$charges->kg_2_for_hr_1;
            $kg3=$charges->kg_3_for_hr_1;
            $kg_val_1=$charges->kg_val_1_for_hr_1;
            $kg_val_2=$charges->kg_val_2_for_hr_1;
            $kg_val_3=$charges->kg_val_3_for_hr_1;
            $kg_val_4=$charges->kg_val_1_for_hr_2;
            $kg_val_5=$charges->kg_val_2_for_hr_2;
            $kg_val_6=$charges->kg_val_3_for_hr_2;
        }
        $home_model=new Homepage();
        $delivery_direction=$home_model->delivery_direction();
        

        return view('home_pages/delivery')->with(compact('home_logo','footer_logo','person_1','person_2','person_3','delivery_image','sms',
        'hr1','hr2','kg1','kg2','kg3','kg_val_1','kg_val_2','kg_val_3','kg_val_4','kg_val_5','kg_val_6','delivery_direction'
        ));
    }
    public function become_rider(){
        $home_model=new Homepage();
        $home_logo=$home_model->home_logo();
        if($home_logo){
            foreach ($home_logo as $logo){
                $home_logo=$logo->img;
            }
        }
        else{
            $home_logo='1.png';
        }
        $footer_logo=$home_model->footer_logo();
        if($footer_logo){
            foreach ($footer_logo as $logo){
                $footer_logo=$logo->img;
            }
        }
        else{
            $footer_logo='3.png';
        }
        $person_1=$home_model->person_1();
        if($person_1){
            foreach ($person_1 as $logo){
                $person_1=$logo->img;
            }
        }
        else{
            $person_1='person_1.png';
        }
        $person_2=$home_model->person_2();
        if($person_2){
            foreach ($person_2 as $logo){
                $person_2=$logo->img;
            }
        }
        else{
            $person_2='person_2.jpg';
        }
        $person_3=$home_model->person_3();
        if($person_3){
            foreach ($person_3 as $logo){
                $person_3=$logo->img;
            }
        }
        else{
            $person_3='person_3.jpg';
        }
        $ride_image=$home_model->ride_image();
        if($ride_image){
            foreach ($ride_image as $logo){
                $ride_image=$logo->img;
                $sms=$logo->dialougue;
            }
        }
        else{
            $ride_image='9.jpg';
            $sms=null;
        }
        
        return view('home_pages/become_rider')->with(compact('home_logo','footer_logo','person_1','person_2','person_3','ride_image','sms'));
    }
    public function ride(){
        $home_model=new Homepage();
        $home_logo=$home_model->home_logo();
        if($home_logo){
            foreach ($home_logo as $logo){
                $home_logo=$logo->img;
            }
        }
        else{
            $home_logo='1.png';
        }
        $footer_logo=$home_model->footer_logo();
        if($footer_logo){
            foreach ($footer_logo as $logo){
                $footer_logo=$logo->img;
            }
        }
        else{
            $footer_logo='3.png';
        }
        $person_1=$home_model->person_1();
        if($person_1){
            foreach ($person_1 as $logo){
                $person_1=$logo->img;
            }
        }
        else{
            $person_1='person_1.png';
        }
        $person_2=$home_model->person_2();
        if($person_2){
            foreach ($person_2 as $logo){
                $person_2=$logo->img;
            }
        }
        else{
            $person_2='person_2.jpg';
        }
        $person_3=$home_model->person_3();
        if($person_3){
            foreach ($person_3 as $logo){
                $person_3=$logo->img;
            }
        }
        else{
            $person_3='person_3.jpg';
        }
        $ride_image=$home_model->ride_image();
        if($ride_image){
            foreach ($ride_image as $logo){
                $ride_image=$logo->img;
                $sms=$logo->dialougue;
            }
        }
        else{
            $ride_image='9.jpg';
            $sms=null;
        }
        return view('home_pages/ride')->with(compact('home_logo','footer_logo','person_1','person_2','person_3','ride_image','sms'));
    }
    public function faq(){
        $home_model=new Homepage();
        $home_logo=$home_model->home_logo();
        if($home_logo){
            foreach ($home_logo as $logo){
                $home_logo=$logo->img;
            }
        }
        else{
            $home_logo='1.png';
        }
        $footer_logo=$home_model->footer_logo();
        if($footer_logo){
            foreach ($footer_logo as $logo){
                $footer_logo=$logo->img;
            }
        }
        else{
            $footer_logo='3.png';
        }
        $person_1=$home_model->person_1();
        if($person_1){
            foreach ($person_1 as $logo){
                $person_1=$logo->img;
            }
        }
        else{
            $person_1='person_1.png';
        }
        $person_2=$home_model->person_2();
        if($person_2){
            foreach ($person_2 as $logo){
                $person_2=$logo->img;
            }
        }
        else{
            $person_2='person_2.jpg';
        }
        $person_3=$home_model->person_3();
        if($person_3){
            foreach ($person_3 as $logo){
                $person_3=$logo->img;
            }
        }
        else{
            $person_3='person_3.jpg';
        }
        $slider_image_1=$home_model->slider_image_1();
        if($slider_image_1){
            foreach ($slider_image_1 as $logo){
                $slider_image_1=$logo->img;
            }
        }
        else{
            $slider_image_1='4.jpg';
        }
        $slider_image_2=$home_model->slider_image_2();
        if($slider_image_2){
            foreach ($slider_image_2 as $logo){
                $slider_image_2=$logo->img;
            }
        }
        else{
            $slider_image_2='5.jpg';
        }
        $slider_image_3=$home_model->slider_image_3();
        if($slider_image_3){
            foreach ($slider_image_3 as $logo){
                $slider_image_3=$logo->img;
            }
        }
        else{
            $slider_image_3='6.jpg';
        }
        $faq=$home_model->faq();
//        var_dump(count($faq));die;
        return view('home_pages/faq')->with(compact('home_logo','footer_logo','person_1','person_2','person_3','faq','slider_image_1','slider_image_2','slider_image_3'));
    }
    public function check_faq_ansr()
    {
//        echo '<pre>';
//        var_dump($_POST);die;
        $home_model = new Homepage();
        $faq = $home_model->faq();
//        echo '<pre>';
//        var_dump($faq[1]);
//        die;
        $x = 0;
        $y = 1;
        $z = 2;
        for ($i = 0; $i < count($faq); $i++) {
            $x++;
            $y++;
            $z++;
            $id[$x] = $faq[$i]->id;
            $given_ansr[$x] = $_POST[$x];
        }
//        echo '<pre>';
//        var_dump($given_ansr);
//        die;
            $faq_ansr = $home_model->check_ansr($id, $given_ansr);
//

            $info = session()->put('correct_ansr', $faq_ansr);
            return redirect('faq');


    }
    
}
