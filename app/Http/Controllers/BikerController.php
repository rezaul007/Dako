<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Biker;
use Illuminate\Support\Facades\Redirect;

class BikerController extends Controller
{
    public function __construct(Request $request)
    {
        $user_id=$request->session()->get('user_id');
        if($user_id==null){
            $error_msg=null;
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request)
    {
        $biker_model = new Biker();
        $biker_info=$biker_model->biker_info($request);
        $total_trip=$biker_model->total_trip();
        $trip=$biker_model->trip_type();
//        echo '<pre>';
//        var_dump($biker_info);
//        echo '<br>';
//        echo 'this is trip';
//        echo '<br>';
//        var_dump($trip);die;
        return view('biker/biker')->with(compact('biker_info','total_trip','trip'));
    }
    public function biker_edit(Request $request,$id)
    {
        $biker_model = new Biker();
        $biker_info=$biker_model->biker_edit($id);
//        echo '<pre>';
//        var_dump($biker_info);
//        die;
        return view('biker/edit_biker')->with(compact('biker_info'));
    }
    public function biker_update(Request $request){
//        echo '<pre>';
//        var_dump($_POST);die;
//        date_default_timezone_set('Asia/Dhaka');
//        $today = Carbon::now();
//        $today->toDateTimeString('Y-m-d h:i:s');
        $id = $_POST['id'];
        $biker_name = $_POST['biker_name'];
        $earn = $_POST['earn'];
        $company_due = $_POST['company_due'];
        $from_due = date('Y-m-d', strtotime($_POST['from_due']));
        $payment_date = date('Y-m-d', strtotime($_POST['payment_date']));
        $biker_model = new Biker();
        $biker_info=$biker_model->update_biker($id,$biker_name,$earn,$company_due,$from_due,$payment_date);
//        echo '<pre>';
//        var_dump($stuff_info);die;
        if($biker_info){
            $msg='Updated successfully.';
            $error_msg=null;
            return redirect('biker')->with(compact('error_msg','msg'));
        }
        else{
            $error_msg='Update unsuccessed';
            $msg=null;
            return redirect('stuff')->with(compact('error_msg','msg'));
        }

    }
    public function biker_daily_ride(){
        $biker_model = new Biker();
        $biker_info=$biker_model->biker_daily_ride();
        $top_ride=$biker_model->top_ride();
        $lifetime_earning=$biker_model->lifetime_earning();
//        echo '<pre>';
//        var_dump($lifetime_earning);die;
        return view('biker/biker_daily_ride')->with(compact('biker_info','top_ride','lifetime_earning'));
    }
    public function biker_daily_ride_details($id){
        $biker_model = new Biker();
        $biker_info=$biker_model->biker_daily_ride();
        $biker_name=$biker_model->biker_name($id);
        $biker_profile=$biker_model->biker_profile($id);
        $biker_details=$biker_model->biker_details($id);
        $top_ride=$biker_model->top_ride();
        $lifetime_earning=$biker_model->lifetime_earning();
//        echo '<pre>';
//        var_dump($biker_profile);die;
        $image=null;
        $nid_img=null;
        $reg_img=null;
        return view('biker/biker_daily_ride_details')->with(compact('biker_info','biker_name','biker_details','top_ride','lifetime_earning','image','nid_img','reg_img','biker_profile'));
        
    }
    public function details_delete($id,$biker_id){
        //echo $id;die;
        $biker_model = new Biker();
        $delete=$biker_model->detail_delete($id);
        return Redirect::to('biker_daily_ride_details/' . $biker_id);
        //echo '<pre>';
//        var_dump($biker_profile);die;
    }
    public function biker_profile_update(){
        $id = $_POST['id'];
        $biker_name = $_POST['biker_name'];
        $bike_name = $_POST['bike_name'];
        $phn = $_POST['biker_phn'];
        $nid = $_POST['nid'];
        $reg = $_POST['reg'];
        $gender = $_POST['gender'];
        $location = $_POST['location'];
        $biker_model = new Biker();
        $biker_update=$biker_model->biker_profil_update($id,$bike_name,$biker_name,$phn,$nid,$reg,$gender,$location);
//            echo 'working';die;
        return Redirect::to('biker_daily_ride_details/' . $id);
        //}

    }
}
