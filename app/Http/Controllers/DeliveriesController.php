<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Deliveries;
use Illuminate\Support\Facades\Redirect;

class DeliveriesController extends Controller
{
    public function __construct(Request $request)
    {
        $user_id=$request->session()->get('user_id');
        if($user_id==null){
            $error_msg=null;
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request)
    {
        $delivery_model = new Deliveries();
        $delivery_info=$delivery_model->delivery_info($request);
        return view('delivery/delivery')->with(compact('delivery_info'));
    }
    public function delivery_details($id)
    {
        $delivery_model = new Deliveries();
        $delivery_info=$delivery_model->delivery_info();
        $delivery_detail=$delivery_model->delivery_details($id);
//        echo '<pre>';
//        var_dump($delivery_info);die;
        return view('delivery/delivery_detail')->with(compact('delivery_info','delivery_detail'));
    }
    public function delivery_details_delete($id){
        $delivery_model = new Deliveries();
        $delivery_delete=$delivery_model->detail_delete($id);
        return Redirect::to('delivery_details/' . $id);
    }
}
