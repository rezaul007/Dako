<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Passenger;
use Illuminate\Support\Facades\Redirect;

class PassengerController extends Controller
{
    public function __construct(Request $request)
    {
        $user_id=$request->session()->get('user_id');
        if($user_id==null){
            $error_msg=null;
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request)
    {
        $passenger_model = new Passenger();
        $passenger_info=$passenger_model->passenger_info($request);
        $lifetime_raiding=$passenger_model->lifetime_raiding();
//        echo '<pre>';
//        var_dump($lifetime_raiding);die;
        return view('passenger/passenger')->with(compact('passenger_info','lifetime_raiding'));
    }
    public function passenger_details($id)
    {
        $passenger_model = new Passenger();
        $passenger_info=$passenger_model->passenger_info();
        $passenger_name=$passenger_model->passenger_name($id);
        $passenger_detail=$passenger_model->passenger_detail($id);
        $passenger_profile=$passenger_model->passenger_profile($id);
        $lifetime_raiding=$passenger_model->lifetime_raiding();
        $image=null;
//        echo '<pre>';
//        var_dump($passenger_detail);die;
        return view('passenger/passenger_detail')->with(compact('passenger_info','lifetime_raiding','passenger_detail','passenger_name','passenger_profile','image'));
    }
    public function passenger_profile_update(){
        $id = $_POST['id'];
        $name = $_POST['passenger_name'];
        $phn = $_POST['phn'];
        $gender = $_POST['gender'];
        $passenger_model = new Passenger();
        $passenger_update=$passenger_model->passenger_profil_update($id,$name,$phn,$gender);
        return Redirect::to('passenger_details/' . $id);
    }
    public function details_delete($id,$passenger_id){
        //echo $id;die;
        $passenger_model = new passenger();
        $delete=$passenger_model->detail_delete($id);
        return Redirect::to('passenger_details/' . $passenger_id);
        //echo '<pre>';
//        var_dump($biker_profile);die;
    }
}
