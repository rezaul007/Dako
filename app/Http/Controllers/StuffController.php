<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Stuff;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class StuffController extends Controller
{
    public function __construct(Request $request)
    {
        $user_id=$request->session()->get('user_id');
        if($user_id==null){
            $error_msg=null;
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request)
    {
        $stuff_model = new Stuff();
        $stuff_info=$stuff_model->stuff_info($request);
//        echo '<pre>';
//        var_dump($stuff_info);die;
        return view('stuff/stuff')->with(compact('stuff_info'));
    }
    public function add_stuff(Request $request)
    {
//        echo '<pre>';
//        var_dump($_POST);die;
        date_default_timezone_set('Asia/Dhaka');
        $today = Carbon::now();
        $today->toDateTimeString('Y-m-d h:i:s');
        if($_POST==null){
            $error_msg=null;
            $msg=null;
            return view('stuff/add_stuff')->with(compact('error_msg','msg'));
        }
        else{
            $stuff_name = $_POST['stuff_name'];
            $permission = $_POST['permission'];
            $stuff_type = $_POST['stuff_type'];
            $status = $_POST['status'];
            $phone = $_POST['phone'];
            $password =md5($_POST['password']) ;
            $stuff_model = new Stuff();
            $check_stuff=$stuff_model->check_stuff($stuff_name);
            if ($check_stuff==null){
                $stuff_add=$stuff_model->stuff_insert($today,$stuff_name,$permission,$stuff_type,$status,$phone,$password);
                if($stuff_add==true){
                    $error_msg=null;
                    $msg="Stuff added successfully";
                    return view('stuff/add_stuff')->with(compact('error_msg','msg'));
                }
                else{
                    $error_msg="Unsuccessful....";
                    $msg=null;
                    return view('stuff/add_stuff')->with(compact('error_msg','msg'));
                }
            }
            else{
                $error_msg="Stuff already exist";
                $msg=null;
                return view('stuff/add_stuff')->with(compact('error_msg','msg'));
            }
        }
        
    }
    public function stuff_edit(Request $request,$id){
//        echo '<pre>';
//        echo "not working";
//        var_dump($_POST);die;
        $stuff_model = new Stuff();
        $stuff_info=$stuff_model->edit_stuff($id);
        $stuff_id=$id;
        return view('stuff/edit_stuff')->with(compact('stuff_info','stuff_id'));

    }
    public function update(Request $request){
//        echo '<pre>';
//        var_dump($_POST);die;
        date_default_timezone_set('Asia/Dhaka');
        $today = Carbon::now();
        $today->toDateTimeString('Y-m-d h:i:s');
        $id = $_POST['id'];
        $stuff_name = $_POST['stuff_name'];
        $permission = $_POST['permission'];
        $stuff_type = $_POST['stuff_type'];
        $status = $_POST['status'];
        $phone = $_POST['phone'];
        if($_POST['password']!=null){
            $password =md5($_POST['password']) ;
        }
        else{
            $password=null;
        }
        
        $stuff_model = new Stuff();
        $stuff_info=$stuff_model->update_stuff($stuff_name,$permission,$stuff_type,$status,$phone,$id,$password,$today);
//        echo '<pre>';
//        var_dump($stuff_info);die;
        if($stuff_info){
            $msg='Updated successfully.';
            $error_msg=null;
            return redirect('stuff')->with(compact('error_msg','msg'));
        }
        else{
            $error_msg='Update unsuccessed';
            $msg=null;
            return redirect('stuff')->with(compact('error_msg','msg'));
        }
        
    }
    public function delete(Request $request,$id){
//        echo '<pre>';
//        var_dump($_POST);die;
        $stuff_model = new Stuff();
        
        $stuff_info=$stuff_model->delete_stuff($id);
//        echo '<pre>';
//        var_dump($stuff_info);die;
        return redirect('stuff');
    }
}
