<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/login', function () {
    $error=session()->get('error_msg');
    if($error==null){
        $error_msg=null;
    }
    else{
        $error_msg=session()->get('error_msg');
    }
    return view('login/login')->with(compact('error_msg'));

});
Route::POST('authentication', 'User_authController@index');
Route::GET('logout', 'User_authController@logout');
Route::POST('add_passenger', 'HomepageController@add_passenger');
Route::POST('add_biker', 'HomepageController@add_biker');
Route::POST('check_faq_ansr', 'HomepageController@check_faq_ansr');
Route::GET('/', 'HomepageController@index');
Route::GET('delivery_page', 'HomepageController@delivery_page');
Route::GET('become_rider', 'HomepageController@become_rider');
Route::GET('ride', 'HomepageController@ride');
Route::GET('faq', 'HomepageController@faq');

Route::GET('biker', 'BikerController@index');
Route::get('biker_edit/{id}','BikerController@biker_edit');
Route::post('edited_biker','BikerController@biker_update');
Route::get('biker_daily_ride','BikerController@biker_daily_ride');
Route::get('biker_daily_ride_details/{id}','BikerController@biker_daily_ride_details');
Route::get('biker_details_delete/{id}/{biker_id}','BikerController@details_delete');
Route::post('edited_biker_profile','BikerController@biker_profile_update');

Route::GET('delivery', 'DeliveriesController@index');
Route::get('delivery_details/{id}','DeliveriesController@delivery_details');
Route::get('delivery_details_delete/{id}','DeliveriesController@delivery_details_delete');

Route::GET('passenger', 'PassengerController@index');
Route::GET('passenger_details/{id}', 'PassengerController@passenger_details');
Route::POST('edited_passenger_profile', 'PassengerController@passenger_profile_update');
Route::get('passenger_details_delete/{id}/{passenger_id}','PassengerController@details_delete');

Route::GET('stuff', 'StuffController@index');
Route::GET('add_stuff', 'StuffController@add_stuff');
Route::POST('add_stuff', 'StuffController@add_stuff');
Route::get('stuff_edit/{id}','StuffController@stuff_edit');
Route::get('stuff_delete/{id}','StuffController@delete');
Route::POST('edited_stuff','StuffController@update');

Route::GET('real_time','Real_timeController@index');

Route::GET('unverify','UnverifyController@index');
Route::get('unverify_edit/{id}','UnverifyController@unverify_edit');
Route::POST('edited_unverify','UnverifyController@update');
Route::get('unverify_delete/{id}','UnverifyController@delete');

Route::GET('settings','SettingsController@index');
Route::POST('edited_sms_api','SettingsController@update_sms_api');
Route::POST('edited_tracking_area','SettingsController@update_tracking_area');
Route::POST('edited_pick_n_off_pick_schedule','SettingsController@update_pick_up_schedule');
Route::POST('edited_pick_n_off_pick_tk','SettingsController@update_pick_up_tk');
Route::POST('edited_pick_n_off_pick_average_tk','SettingsController@update_pick_up_average_tk');
Route::POST('edited_ride_request','SettingsController@update_ride_request');
Route::POST('edited_non_average_refaral','SettingsController@update_non_average_refaral');
Route::POST('edited_average_refaral','SettingsController@update_average_refaral');
Route::POST('edited_request_stage','SettingsController@update_request_stage');
Route::POST('edited_account_info','SettingsController@update_account_info');
Route::POST('search_block_person','SettingsController@search_block_person');
Route::POST('block_person','SettingsController@block_person');
Route::POST('edited_faq','SettingsController@insert_faq');
Route::POST('edited_whole_site_settings','SettingsController@update_whole_site_settings');
Route::POST('edited_site_image_n_logo','SettingsController@update_site_image_n_logo');
Route::POST('edited_auto_chat','SettingsController@update_auto_chat');
Route::POST('edited_chat_person','SettingsController@update_chat_person');
Route::POST('edited_dhaka_hours','SettingsController@update_dhaka_hours');
Route::POST('edited_dhaka_kg','SettingsController@update_dhaka_kg');
Route::POST('edited_dhaka_kg_val','SettingsController@update_dhaka_kg_val');
Route::POST('edited_other_hours','SettingsController@update_other_hours');
Route::POST('edited_other_kg','SettingsController@update_other_kg');
Route::POST('edited_ctg_kg_val','SettingsController@update_ctg_kg_val');
Route::POST('edited_raj_kg_val','SettingsController@update_raj_kg_val');
Route::POST('edited_khul_kg_val','SettingsController@update_khul_kg_val');
Route::POST('edited_syl_kg_val','SettingsController@update_syl_kg_val');
Route::POST('edited_overall_kg_val','SettingsController@update_overall_kg_val');
Route::POST('edited_direction_sms','SettingsController@update_direction_sms');
