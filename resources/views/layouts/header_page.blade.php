<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Parapar | Admin</title>



    <link rel="stylesheet" href="{{ URL::asset('theme/plugins/datatables/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/plugins/iCheck/flat/blue.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/plugins/morris/morris.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/plugins/iCheck/square/blue.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="{!! url('dashboard') !!}" class="logo">
            <span class="logo-lg"><b>Parapar | Dashboard</b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?
                            $image=session()->get('image');
                            $name=session()->get('name');
                            if($image!=null){?>
                           <img src="{{ URL::asset('uploaded_image/user_profile/'.$image) }}" class="user-image" alt="User Image">
                           <span class="hidden-xs"><? echo $name;?></span><?} else{?>
                            <span class="hidden-xs"><? echo $name;?></span>
                            <img src="{{ URL::asset('uploaded_image/user_profile/default.png') }}" class="user-image" alt="User Image">
                            <?}?>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                {{--<div class="pull-left">--}}
                                    {{--<a href="{!! url('profile') !!}" class="btn btn-default btn-flat">Profile</a>--}}
                                {{--</div>--}}
                                <div class="pull-right">
                                    <a href="{!! url('logout') !!}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>