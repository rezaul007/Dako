
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2
    </div>
    <strong>Copyright &copy;<a href="">Parapar</a>.</strong> All rights
    reserved.
</footer>
<div class="control-sidebar-bg"></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!--<script src="--><?php //echo base_url()?><!--/resources/theme/plugins/morris/morris.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ URL::asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ URL::asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/knob/jquery.knob.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ URL::asset('theme/dist/js/app.min.js') }}"></script>

<script src="{{ URL::asset('theme/dist/js/pages/dashboard.js') }}"></script>
<script src="{{ URL::asset('theme/dist/js/demo.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>


</html>
