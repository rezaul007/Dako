<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <?
                $image=session()->get('image');
                $name=session()->get('name');
                if($image!=null){?>
                <img src="{{ URL::asset('uploaded_image/user_profile/') }}<?$image;?>" class="img-circle" alt="User Image"><?} else{?>
                    <img src="{{ URL::asset('uploaded_image/user_profile/default.png') }}" class="img-circle" alt="User Image"><?}?>
                </div>
            <div class="pull-left info">
                <p><? echo $name;?></p>
        </div>
            </div>
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cube"></i></i> <span>Real Time</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('real_time') !!}"><i class="fa fa-circle-o"></i>Real Time</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i></i> <span>Biker</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('biker_daily_ride') !!}"><i class="fa fa-circle-o"></i>Biker Daily ride</a></li>
                    <li><a href="{!! url('biker') !!}"><i class="fa fa-circle-o"></i>Biker payment</a></li>
                    <li><a href="{!! url('unverify') !!}"><i class="fa fa-circle-o"></i>Unverify</a></li>
                    <li><a href="{!! url('earning') !!}"><i class="fa fa-circle-o"></i>Earning</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i></i> <span>Passenger</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href={{ url('passenger') }}><i class="fa fa-circle-o"></i>Passenger List</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cube"></i></i> <span>Deliveries</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('delivery') !!}"><i class="fa fa-circle-o"></i>Deliveries</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cube"></i></i> <span>Stuff</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('stuff') !!}"><i class="fa fa-circle-o"></i>Stuff List</a></li>
                    <li><a href="{!! url('add_stuff') !!}"><i class="fa fa-circle-o"></i>Add Stuff</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cube"></i></i> <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('settings') !!}"><i class="fa fa-circle-o"></i>Settings</a></li>
                </ul>
            </li>

        </ul>
    </section>
</aside>
@yield('content')

