
@section('content')
    <div class="content-wrapper">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Real Time</b></h3>
        </div><br>
        <div class="box-body">
            <div style="float:left;width: 45%">
                <table  class="table table-striped">
                    <thead>
                    <tr>
                        <th>Dhaka</th>
                        <th></th>
                        <th></th>
                        <th callspan='2' style="float: right">Passenger/biker total = <?php foreach ($passenger_on_dhaka as $d){echo $d->info;} ?>/<?php foreach ($biker_on_dhaka as $d){echo $d->info;} ?></th>
                        <th></th>
                    </tr>
                    </thead>
                    </table>
                <table  class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Motijhil</td>
                        <td>Jatrabari</td>
                        <td>Old dhaka</td>
                        <td>Malibag</td>
                        <td>Badda</td>
                    </tr>
                    <tr>
                        <td><?php foreach ($passenger_on_motijhil as $d){echo $d->info;} ?>/<?php foreach ($biker_on_motijhil as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_jatrabari as $d){echo $d->info;} ?>/<?php foreach ($biker_on_jatrabari as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_old_dhaka as $d){echo $d->info;} ?>/<?php foreach ($biker_on_old_dhaka as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_malibag as $d){echo $d->info;} ?>/<?php foreach ($biker_on_malibag as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_badda as $d){echo $d->info;} ?>/<?php foreach ($biker_on_badda as $d){echo $d->info;} ?></td>
                    </tr>
                    <tr>
                        <td>Dhanmondi</td>
                        <td>Mirpur</td>
                        <td>Md pur</td>
                        <td>Banani</td>
                        <td>Uttara</td>
                    </tr>
                    <tr>
                        <td><?php foreach ($passenger_on_dhanmondi as $d){echo $d->info;} ?>/<?php foreach ($biker_on_dhanmondi as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_mirpur as $d){echo $d->info;} ?>/<?php foreach ($biker_on_mirpur as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_mdpur as $d){echo $d->info;} ?>/<?php foreach ($biker_on_mdpur as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_banani as $d){echo $d->info;} ?>/<?php foreach ($biker_on_banani as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_uttara as $d){echo $d->info;} ?>/<?php foreach ($biker_on_uttara as $d){echo $d->info;} ?></td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <div style="float:right;width: 45%">
                <table  class="table table-striped">
                    <thead>
                    <tr>
                        <th >Chittagong</th>
                        <th ></th>
                        <th></th>
                        <th></th>
                        <th callspan='2' style="float: right">Passenger/biker total <?php foreach ($passenger_on_ctg as $d){echo $d->info;} ?>/<?php foreach ($biker_on_ctg as $d){echo $d->info;} ?></th>
                    </tr>
                    </thead>
                    </table>
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Motijhil</td>
                        <td>Jatrabari</td>
                        <td>Old dhaka</td>
                        <td>Malibag</td>
                        <td>Badda</td>
                    </tr>
                    <tr>
                    <tr>
                        <td><?php foreach ($passenger_on_motijhil as $d){echo $d->info;} ?>/<?php foreach ($biker_on_motijhil as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_jatrabari as $d){echo $d->info;} ?>/<?php foreach ($biker_on_jatrabari as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_old_dhaka as $d){echo $d->info;} ?>/<?php foreach ($biker_on_old_dhaka as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_malibag as $d){echo $d->info;} ?>/<?php foreach ($biker_on_malibag as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_badda as $d){echo $d->info;} ?>/<?php foreach ($biker_on_badda as $d){echo $d->info;} ?></td>
                    </tr>
                    </tr>
                    <tr>
                        <td>Dhanmondi</td>
                        <td>Mirpur</td>
                        <td>Md pur</td>
                        <td>Banani</td>
                        <td>Uttara</td>
                    </tr>
                    <tr>
                    <tr>
                        <td><?php foreach ($passenger_on_dhanmondi as $d){echo $d->info;} ?>/<?php foreach ($biker_on_dhanmondi as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_mirpur as $d){echo $d->info;} ?>/<?php foreach ($biker_on_mirpur as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_mdpur as $d){echo $d->info;} ?>/<?php foreach ($biker_on_mdpur as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_banani as $d){echo $d->info;} ?>/<?php foreach ($biker_on_banani as $d){echo $d->info;} ?></td>
                        <td><?php foreach ($passenger_on_uttara as $d){echo $d->info;} ?>/<?php foreach ($biker_on_uttara as $d){echo $d->info;} ?></td>
                    </tr>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Biker Data</b></h3>
        </div><br>
        <div class="box-body">
            <table style="float: left;margin:0 10px;width: 10%">
                <th>Total biker</th>
                <tr>
                    <td>
                        Dhaka <?php foreach ($biker_on_dhaka as $d){echo $d->info;} ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Chtg <?php foreach ($biker_on_ctg as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 10%">
                <th>Total passenger</th>
                <tr>
                    <td>
                        Dhaka <?php foreach ($passenger_on_dhaka as $d){echo $d->info;} ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Chtg <?php foreach ($passenger_on_ctg as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 10%">
                <th>Now online</th>
                <tr>
                    <td>
                        Dhaka <?php foreach ($biker_online_dhaka as $d){echo $d->info;} ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Chtg <?php foreach ($biker_online_ctg as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left; margin:0 10px;width: 10%">
                <th>Now onride</th>
                <tr>
                    <td>
                        Dhaka <?php foreach ($biker_onride_dhaka as $d){echo $d->info;} ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Chtg <?php foreach ($biker_onride_ctg as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left; margin:0 10px;width: 10%">
                <th>Unverified</th>
                <tr>
                    <td>
                        Dhaka <?php foreach ($biker_unverify_dhaka as $d){echo $d->info;} ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Chtg <?php foreach ($biker_unverify_dhaka as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;width: 10%">
                <th>Gender</th>
                <tr>
                    <td>
                        Male <?php foreach ($male_biker as $d){echo $d->info;} ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Female <?php foreach ($female_biker as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Delivery Data</b></h3>
        </div><br>
        <div class="box-body">
            <table style="float: left;margin:0 10px;width: 10%">
                <th>Total Delivery</th>
                <tr>
                    <td>
                        <?php foreach ($total_delivery as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 12%">
                <th>Product on the way</th>
                <tr>
                    <td>
                        <?php foreach ($on_way as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 10%">
                <th>Due amount</th>
                <tr>
                    <td>
                        <?php foreach ($delivery_due_amount as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 10%">
                <th>Back in store</th>
                <tr>
                    <td>
                        980
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Biker today Data</b></h3>
        </div><br>
        <div class="box-body">
            <table style="float: left;margin:0 10px;width: 10%">
                <th>Total ride</th>
                <tr>
                    <td>
                        <?php foreach ($total_ride as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 10%">
                <th>total earn</th>
                <tr>
                    <td>
                        <?php foreach ($total_earn as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 15%">
                <th>today's company earn</th>
                <tr>
                    <td>
                        <?php foreach ($total_company_earn as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 15%">
                <th>today's company refaral</th>
                <tr>
                    <td>
                        <?php foreach ($total_company_refala as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Delivery today Data</b></h3>
        </div>
        <div class="box-body">
            <table style="float: left;margin:0 10px;width: 10%">
                <th>Today's earn</th>
                <tr>
                    <td>
                        <?php foreach ($total_delivery_earn as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 10%">
                <th>Delivery new</th>
                <tr>
                    <td>
                        <?php foreach ($new_delivery as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 15%">
                <th>Today mirpur order</th>
                <tr>
                    <td>
                        <?php foreach ($today_mirpur_order as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 15%">
                <th>Today md pur order</th>
                <tr>
                    <td>
                        <?php foreach ($today_mdpur_order as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="box-body" style="padding-left: 23.5%">
            <table style="float:left;margin:0 10px;width: 20%">
                <th>Today motijhil order</th>
                <tr>
                    <td>
                        <?php foreach ($today_motijhil_order as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 15%">
                <th>Today malibag order</th>
                <tr>
                    <td>
                        <?php foreach ($today_malibag_order as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="box-body" style="padding-left: 23.5%">
            <table style="float:left;margin:0 10px;width: 20%">
                <th>Today old dhaka order</th>
                <tr>
                    <td>
                        <?php foreach ($today_old_dhaka_order as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
            <table style="float:left;margin:0 10px;width: 15%">
                <th>Today badda order</th>
                <tr>
                    <td>
                        <?php foreach ($today_badda_order as $d){echo $d->info;} ?>
                    </td>
                </tr>
            </table>
        </div>
        <br>
    </div>
    <script>
        $(document).ready(function() {
            $('#biker_table').DataTable( {
                "scrollX": true
            } );
        } );
    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')