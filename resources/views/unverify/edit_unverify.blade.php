@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Unverify Edition</h3>
                        </div>

                        <form action="{{ url('edited_unverify') }}" method="post" role="form" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                            <div class="box-body">
                                <?php foreach ($biker_info as $info){?>
                                <div class="form-group">
                                    <label>Biker Name</label><span style="color: #ff0000">*</span>
                                    <input name="biker_name" required type="text" class="form-control" value="<?php echo $info->biker_name; ?>" >
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Phone</label><span style="color: #ff0000">*</span>
                                    <input name="phone" required type="text" class="form-control" value="<?php echo $info->biker_phn; ?>">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Area</label><span style="color: #ff0000">*</span>
                                    <input name="area" required type="text" class="form-control" value="<?php echo $info->area; ?>">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                    <div class="form-group">
                                        <label>Gender</label><span style="color: #ff0000">*</span>
                                        <input name="gender" required type="radio" value="male" <?php if($info->gender=='male'){echo 'checked';}?>>Male &nbsp; &nbsp;
                                        <input name="gender" required type="radio"  value="female" <?php if($info->gender=='female'){echo 'checked';}?>>Female

                                    </div>

                                    <div class="form-group">
                                        <label>Nid</label><span style="color: #ff0000">*</span>&nbsp; &nbsp; &nbsp; &nbsp;
                                        <input name="nid" id="nid" onclick="yesnoCheck()" required type="radio" value="Yes" <?php if($info->biker_nid!=null){echo 'checked';}?>>&nbsp; Yes &nbsp; &nbsp;
                                        <input name="nid" required onclick="yesnoCheck()" type="radio" value="No" <?php if($info->biker_nid==null){echo 'checked';}?>>No
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                    <div class="form-group" id="nid_field" style="<?php if($info->biker_nid!=null){echo 'display:block';} else{echo 'display:none';}?>">
                                        <input type="text" id="nid_input" name="nid"  class="form-control" value="<?php echo $info->biker_nid;?>" placeholder="nid number">
                                    </div>

                                    <div class="form-group">
                                        <label>Reg</label><span style="color: #ff0000">*</span>&nbsp; &nbsp; &nbsp; &nbsp;
                                        <input name="reg" id="reg" onclick="yesnoCheck2()" required type="radio" value="Yes" <?php if($info->biker_reg!=null){echo 'checked';}?>>&nbsp; Yes &nbsp; &nbsp;
                                        <input name="reg" onclick="yesnoCheck2()" required type="radio" value="No" <?php if($info->biker_reg==null){echo 'checked';}?>>No
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>

                                    <div class="form-group" id="reg_field" style="<?php if($info->biker_reg!=null){echo 'display:block';} else{echo 'display:none';}?>">
                                        <input type="text" name="reg" id="reg_input"  class="form-control" value="<?php echo $info->biker_reg;?>" placeholder="reg number">
                                    </div>

                                <div class="form-group">
                                    <label>Location</label><span style="color: #ff0000">*</span>
                                    <input type="text" name="location" required class="form-control" value="<?php echo $info->location; ?>">
                                    <span></span>
                                </div>
                                    <div class="form-group">
                                        <label>Photo</label><span style="color: #ff0000">*</span>&nbsp; &nbsp;
                                        <input name="img" id="img" onclick="yesnoCheck3()" required type="radio" value="Yes" <?php if($info->biker_img!=null){echo 'checked';}?>>&nbsp; Yes &nbsp; &nbsp;
                                        <input name="img" onclick="yesnoCheck3()" required type="radio" value="No" <?php if($info->biker_img==null){echo 'checked';}?>>No
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                    <div class="form-group" id="img_field" style="<?php if($info->biker_img!=null){echo 'display:block';} else{echo 'display:none';}?>">
                                        <input type="file" name="img"  id="photo_input"  value="<?php echo $info->biker_img;?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Bike model</label><span style="color: #ff0000">*</span>
                                        <input type="text" name="model" required class="form-control" value="<?php echo $info->bike_model; ?>">
                                        <span></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Call Biker</label><span style="color: #ff0000">*</span>
                                        <input type="text" name="call_biker" required class="form-control" value="<?php echo $info->call_biker; ?>">
                                        <span></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Call from</label><span style="color: #ff0000">*</span>
                                        <input type="text" name="call_from" required class="form-control" value="<?php echo $info->call_from; ?>">
                                        <span></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Test result</label><span style="color: #ff0000">*</span>
                                        <input type="text" name="test_result" required class="form-control" value="<?php echo $info->test_result; ?>">
                                        <span></span>
                                    </div>
                                    <?php }?>
                                <div class="box-footer">
                                    <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
                                    <button  type="submit" class="btn btn-info pull-right">Update unverifier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        function yesnoCheck() {
            if (document.getElementById('nid').checked) {
                document.getElementById('nid_field').style.display = 'block';
            }
            else {
                document.getElementById('nid_field').style.display = 'none';
                document.getElementById('nid_input').value = null;
            }
        }

    </script>
    <script>
        function yesnoCheck2() {
            if (document.getElementById('reg').checked) {
                document.getElementById('reg_field').style.display = 'block';
            }
            else {
                document.getElementById('reg_field').style.display = 'none';
                document.getElementById('reg_input').value = null;
            }
        }

    </script>
    <script>
        function yesnoCheck3() {
            if (document.getElementById('img').checked) {
                document.getElementById('img_field').style.display = 'block';
            }
            else {
                document.getElementById('img_field').style.display = 'none';
                document.getElementById('photo_input').value = null;
            }
        }

    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')