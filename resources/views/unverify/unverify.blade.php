
@section('content')
    <div class="content-wrapper">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Unverify</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table" class="table table-bordered table-striped">
                <thead>
                <tr>

                    <th>Biker ID</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Area</th>
                    <th>Gender</th>
                    <th>Nid</th>
                    <th>Reg</th>
                    <th>Location</th>
                    <th>Photo</th>
                    <th>Bike model</th>
                    <th>Call Biker?</th>
                    <th>From</th>
                    <th>Test result</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($verify_info as $biker) {
                echo "<tr>";

                echo "<td>{$biker->id}</td>";
                echo "<td class='tdStyle'>{$biker->biker_name}</td>";
                echo "<td class='tdStyle'>{$biker->biker_phn}</td>";
                echo "<td class='tdStyle'>{$biker->area}</td>";
                echo "<td class='tdStyle'>{$biker->gender}</td>";?>
                        <?php
                if($biker->biker_nid!=null){?>
                    <td>Yes</td>
                <?php
                        }
                        else{?>
                            <td>No</td>
                   <?php }
                           if($biker->biker_reg!=null){?>
                <td>Yes</td>
                <?php
                }
                else{?>
                <td>No</td>
                <?php }
                echo "<td>{$biker->location}</td>";
                if($biker->biker_img!=null){
                ?>
                <td>Yes</td>
                <?php
                }
                else{?>
                <td>No</td>
                <?php }
                echo "<td>{$biker->bike_model}</td>";
                echo "<td>{$biker->call_biker}</td>";
                echo "<td>{$biker->call_from}</td>";
                echo "<td>{$biker->test_result}</td>";
                ?>
                <td><a href="{!! url('unverify_edit',array('id'=>$biker->id)) !!}" class='btn btn-block btn-success'>Edit</a></td>
                <td><a href="{!! url('unverify_delete',array('id'=>$biker->id)) !!}" class='btn btn-block btn-success'>Delete</a></td>
                <?}?>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#biker_table').DataTable( {
                "scrollX": true
            } );
        } );
    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')