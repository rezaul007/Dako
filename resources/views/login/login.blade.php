<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ URL::asset('theme/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/plugins/iCheck/square/blue.css') }}">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div>{{$error_msg}}</div>
    <div class="login-box-body">
        <p class="login-box-msg"><b style="font-size:x-large">Dako Admin</b></p>
        <form action="{!! url('authentication') !!}", method="post">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="pass" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4" style="float: right">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" >Sign In</button>
                </div>
            </div>
        </form></div>
</div>
<script src="{{ URL::asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ URL::asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/iCheck/icheck.min.js') }}"></script>
</body>
</html>
