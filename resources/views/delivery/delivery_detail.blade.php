
@section('content')
    <div class="content-wrapper">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Deliveries</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Trac_id</th>
                    <th>Bill</th>
                    <th>Send Time</th>
                    <th>Receiver Details</th>
                    <th>Product Details</th>
                    <th>collect near</th>
                    <th>delivery_crg_pay</th>
                    <th>rcv_amount</th>
                    <th>cod_ccd</th>
                    <th>parapar_charge</th>
                    <th>mrct_am_rcv</th>
                    <th>parapar_am_rcv</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($delivery_info as $info) {
                    echo "<tr>";
                    echo "<td class='tdStyle'>{$info->trac_id}</td>";
                    echo "<td>{$info->bill_no}</td>";
                    echo "<td>{$info->send_time}</td>";
                    echo "<td>{$info->receiver_details}</td>";
                    echo "<td>{$info->product_details}</td>";
                    echo "<td>{$info->place}</td>";
                    echo "<td>{$info->delivery_crg_pay}</td>";
                    echo "<td>{$info->rcv_amount}</td>";
                    echo "<td>{$info->cod_ccd}</td>";
                    echo "<td>{$info->parapar_charge}</td>";
                    echo "<td>{$info->mrct_am_rcv}</td>";
                    echo "<td>{$info->parapar_am_rcv}</td>"?>
                <td><a href="{!! url('delivery_details',array('id'=>$info->id)) !!}" class='btn btn-block btn-success'>Details</a></td>
                <?}?>
                </tbody>
            </table>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Delivery Details</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table4" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Bill no.</th>
                    <th>Product</th>
                    <th>Receiver Add.</th>
                    <th>Receiver phn</th>
                    <th>Place</th>
                    <th>Collect hub</th>
                    <th>Chrg pay</th>
                    <th>COD/CCD amount</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($delivery_detail as $info) {
                echo "<tr>";
                echo "<td>{$info->bill_no}</td>";
                echo "<td>{$info->product_details}</td>";
                echo "<td>{$info->receiver_details}</td>";
                echo "<td>{$info->rcver_phn}</td>";
                echo "<td>{$info->place}</td>";
                echo "<td>{$info->collect_hub}</td>";
                echo "<td>{$info->parapar_charge}</td>";
                echo "<td>{$info->cod_ccd}</td>";?>
                <td><a href="{!! url('delivery_details_delete',array('id'=>$info->id)) !!}" class='btn btn-block btn-success'>Delete</a></td>
                <?}?>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#biker_table').DataTable( {
                "scrollX": true
            } );
        } );
    </script>
    <script>
        $(document).ready(function() {
            $('#biker_table4').DataTable( {
                "scrollX": true
            } );
        } );
    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')