
@section('content')
    <div class="content-wrapper">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Settings</b></h3>
        </div><br>
        <div class="box-body">
            <form name="sms_api" action="{{ url('edited_sms_api') }}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SMS API Setup</th>
                    <th>Total sms</th>
                    <th>Unused sms</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($sms_info as $info) {
                echo "<tr>";?>
                <td><input type="hidden" name="id" value="<?php echo $info->id ?>"></td>
                <td><input type="text" required name="total_sms" value="<?php echo $info->total_sms ?>"></td>
                <td><input type="text" required name="unused_sms" value="<?php echo $info->unused_sms ?>"></td>
                <td><button  type="submit" class="btn btn-info pull-right">Update</button></td>
                <?php }?>
                </tbody>
            </table>
                </form>
        </div>
        <br>
        <div class="box-body">
            <form name="tracking_area" action="{{ url('edited_tracking_area') }}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <table id="biker_table" class="table table-bordered table-striped">
                    <tbody>
                    <?php
                    foreach($area_info as $info) {
                    echo "<tr>";?>
                    <td>Passenger/ Biker tracking how many area?</td>
                    <td><input type="hidden" name="id" value="<?php echo $info->id ?>"></td>
                    <td></td>
                    <td><input type="text" required name="area" value="<?php echo $info->area ?>"></td>
                    <td><button  type="submit" class="btn btn-info pull-right">Update</button></td>
                    <?php }?>
                    </tbody>
                </table>
            </form>
        </div>
        <br>
        <div class="box-body">
                <div style="overflow-y:scroll;overflow:auto">
                <table id="example" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Base</th>
                        <th colspan="5">Pick/offpick schedule & rate</th>
                        <th>/min rate</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <form name="sms_api" action="{{ url('edited_pick_n_off_pick_schedule') }}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <?php
                    echo "<tr>";?>
                    <td></td>
                    <td></td>
                    <?php
                        $serial=0;
                        foreach($pick_time_info as $info) {
                        $serial++;
                        ?>
                    <td>
                        <input type="hidden" name="id/<?php echo $serial;?>" value="<?php echo $info->id ?>">
                        <input type="text" required name="<?php echo $serial;?>/schedule" value="<?php echo $info->schedule ?>"></td>
                    <?php }?>
                    <td></td>
                    <td><button  type="submit" class="btn btn-info pull-right">Update</button></td>
                        </form>
                    <?php
                    echo "<tr>";?>
                    <form name="sms_api" action="{{ url('edited_pick_n_off_pick_tk') }}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <td><input id="non_average"  type="radio" name="average_time_status" checked="<?php foreach($pick_time_info as $info) { if($info->average_time_status=="0"){echo 'checked';} break;} ?> " value="0" onclick="average();"></td>
                    <td><?php foreach($pick_time_info as $info) { echo $info->base; break;}?></td>
                    <?php
                        $serial=0;
                        foreach($pick_time_info as $info) {
                        $serial++;
                        ?>
                    <td>
                        <input type="hidden" name="id/<?php echo $serial;?>" value="<?php echo $info->id ?>">
                        <input type="text" required name="<?php echo $serial;?>/tk" value="<?php echo 'TK '.$info->tk ?>"></td>
                    <?php }?>
                    <td><input type="text" required name="per_min_rate" value="<?php echo 'TK '.$info->per_min_rate_non_average ?>"></td>
                    <td><button  type="submit" class="btn btn-info pull-right">Update</button></td>
                        </form>



                    <?php
                    echo "<tr>";?>
                    <form name="sms_api" action="{{ url('edited_pick_n_off_pick_average_tk') }}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <td><input id="average_tk"  type="radio" name="average_time_status" <?php foreach($pick_time_info as $info) { if($info->average_time_status=="1"){echo 'checked';} break;} ?> value="1" onclick="non_average()"></td>
                    <td><?php foreach($pick_time_info as $info) { echo $info->base; break;}?></td>
                    <?php foreach($pick_time_info as $info) {?>
                    <td colspan="5">
                        <input type="hidden" name="id" value="<?php echo $info->id ?>">
                        <input type="text" required name="average_tk" value="<?php echo 'TK '.$info->average_tk; ?>" style="width: 100%;text-align: center"></td>
                    <?php break;}?>
                    <td><input type="text" required name="per_min_rate" value="<?php echo 'TK '.$info->per_min_rate_average ?>"></td>
                    <td><button  type="submit" class="btn btn-info pull-right">Update</button></td>
                        </form>
                    </tbody>
                </table>
                    </div>
        </div>
        <br>
        <div class="box-body">
            <form name="sms_api" action="{{ url('edited_ride_request') }}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th colspan="3">Ride request accept</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($ride_request as $info) {
                    echo "<tr>";?>
                    <td><input type="hidden" name="id/<?php echo $info->id ?>" value="<?php echo $info->id ?>"></td>
                    <td><input type="text" required name="area/<?php echo $info->id ?>" value="<?php echo $info->area ?>"></td>
                    <td><input type="text" required name="time/<?php echo $info->id ?>" value="<?php echo $info->time ?>"></td>
                    <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                    <?php }?>
                    </tbody>
                </table>
            </form>
        </div>
        <br>
        <div class="box-body">
            <div style="overflow-y:scroll;overflow:auto">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th colspan="10">Refaral how many?</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <?php
                    foreach($refaral_how_many as $info) {?>

                        <td colspan="5" style="text-align:left;"><?php echo $info->area;?></td>

                    <?php }?>
                    </tr>
                    <form name="non_average_refaral" action="{{ url('edited_non_average_refaral') }}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <tr>
                        <?php
                            $serial=0;
                        foreach($refaral_how_many as $info) {
                        $serial++;
                        ?>
                        <td ><input type="radio"  id="non_average_refaral/<?php echo $serial;?>" value="<?php if($info->average_status==1){echo '1';} else{echo '0';} ?>" <?php if($info->average_status==0){echo 'checked';} ?> name="non_average_refaral/<?php echo $serial;?>" onclick="uncheck_<?php echo $serial;?>()"></td>
                        <td ><input type="hidden" name="id/<?php echo $serial;?>" value="<?php echo $serial;?>"></td>
                        <td ><input type="text" required name="first/<?php echo $serial;?>" value="<?php echo $info->first;?>"></td>
                        <td ><input type="text" required name="after/<?php echo $serial;?>" value="<?php echo $info->after;?>"></td>
                        <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        <?php }?>
                    </tr>
                        </form>
                    <form name="average_refaral" action="{{ url('edited_average_refaral') }}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <tr>
                        <?php
                        $serial=0;
                        foreach($refaral_how_many as $info) {
                        $serial++;
                        ?>
                        <td ><input type="radio" id="average_refaral/<?php echo $serial;?>" value="<?php if($info->average_status==0){echo '0';} else{echo '1';} ?>" <?php if($info->average_status==1){echo 'checked';} ?>  name="average_refaral/<?php echo $serial;?>" onclick="offcheck_<?php echo $serial;?>()"></td>
                        <td ><input type="hidden" name="id/<?php echo $serial;?>" value="<?php echo $serial;?>" ></td>
                        <td colspan="2"><input style="width: 100%;text-align: center" type="text" required name="average/<?php echo $serial;?>" value="<?php echo $info->average;?>"></td>
                        <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        <?php }?>
                    </tr>
                        </form>
                    </tbody>
                </table>
            </form>
                </div>
        </div>
        <br>
        <div class="box-body">
            <form name="sms_api" action="{{ url('edited_request_stage') }}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <table class="table table-bordered table-striped" style="width: 50%">
                    <thead>
                    <tr>
                        <th>Ride request count from which stage?</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($request_count_stage as $info) {
                    echo "<tr>";?>
                    <input type="hidden" name="id/<?php echo $info->id ?>" value="<?php echo $info->id ?>" style="width: 5%">
                    <td><input type="radio" name="status" <?php if($info->active_status==1)echo 'checked';?> value="<?php echo $info->id ?>">&nbsp; &nbsp;  <?php echo $info->request; ?></td>
                    <?php }
                    echo "<tr>";
                    ?>

                    <td><button  type="submit" class="btn btn-info pull-right">Update</button></td>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="box-body">
            <form name="tracking_area" action="{{ url('edited_account_info') }}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <table class="table table-bordered table-striped" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Our accounts information</th>
                    </tr>
                    </thead>
                    <tbody >
                    <?php
                    foreach($account_info as $info) {
                    echo "<tr>";?>
                    <input type="hidden" name="id/<?php echo $info->id ?>" value="<?php echo $info->id ?>" >
                    <td><input class="form-control" type="text" required name="account_name/<?php echo $info->id ?>" value="<?php echo $info->account_name ?>"></td>
                    <td><input class="form-control" type="text" required name="account_no/<?php echo $info->id ?>" value="<?php echo $info->account_no ?>"></td>
                    <?php }?>
                    <td><button  type="submit" class="btn btn-info pull-right">Update</button></td>
                    </tbody>
                </table>
            </form>
        </div>
        <br>
        <div class="box-body">
                <table class="table table-bordered table-striped" style="width: 100%">
                    <thead>
                    <tr>
                        <th colspan="2">Block Rider/passenger</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    echo "<tr>";?>
                    <form name="sms_api" action="{{ url('search_block_person') }}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <td colspan="2"><input class="form-control" type="text" required name="info" placeholder="mobile no./biker reg no."></td>
                    <td ><button  type="submit" class="btn btn-info pull-left">Search</button></td>
                        </form>
                    <?php
                    echo "<tr>";
                    if ($person_info!=null){
                    ?>
                    <form name="sms_api" action="{{ url('block_person') }}" method="post">
                        <input  type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <td colspan="2"><input class="form-control" required type="text" name="info" value="<?php echo $person_info;?>"></td>
                    <td><button  type="submit" class="btn btn-info pull-left">Block</button></td>
                        </form>
                    <?php }?>
                    </tbody>
                </table>
            </form>
        </div>
        <br>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Whole site/app base settings</b></h3>
        </div>
        <div style="overflow-y:scroll;overflow:auto">
        <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th colspan="4">App or site advert</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Position</th>
                        <th>Image</th>
                        <th>site link/adesnse script code</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($whole_site_info as $info) {
                    echo "<tr>";?>
                    <form name="sms_api" action="{{ url('edited_whole_site_settings') }}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <td><input type="hidden" name="id" value="<?php echo $info->id ?>"></td>
                    <td><input type="checkbox" name="position_active"  <?php if($info->active_status==1){echo 'checked';} ?>></td>
                    <td><input type="text" required name="position" value="<?php echo $info->position ?>"></td>
                        <?php if( $info->img==null){?>
                    <td><input type="file"  name="img"></td>
                        <?php }
                         else{?>
                        <td>
                            <level><?php echo $info->img ;?></level>
                            <input type="file"  name="img" value="1.png"></td>
                        <?php }?>
                    <td><input type="text" required name="link" value="<?php echo $info->sitelink_or_script ?>"></td>
                    <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                    <?php }?>
                    </tbody>
                </table>
        </div>
            </div>
        <br>
        <div class="box-body">
            <form name="faq_ansr" action="{{ url('edited_faq') }}" method="post" >
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th colspan="4">FAQ questions</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    echo "<tr>";?>
                    <td>Click to the right answer on radio button<br>
                        <input class="form-control" required type="text" name="qstn" placeholder="question"></td>
                    <td><input id="radio_1"  type="radio" value="1" name="right/1" onclick="checkother_1()">
                        <input class="form-control" required type="text" name="ansr1" placeholder="option1" ></td>
                    <td><input id="radio_2" type="radio" value="2" name="right/2" onclick="checkother_2()">
                        <input class="form-control" required type="text" name="ansr2" placeholder="option1"></td>
                    <td><input id="radio_3" type="radio" value="3" name="right/3"onclick="checkother_3()">
                        <input class="form-control" required type="text" name="ansr3" placeholder="option1"></td>
                    <td><input type="submit" class="btn btn-info pull-left" value="save" onclick="return checkoption()"></td>
                    </tbody>
                </table>
            </form>
        </div>
        <br>
        <div style="overflow-y:scroll;overflow:auto">
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th colspan="4">Site image & logo</th>
                </tr>
                <tr>
                    <th>Position name</th>
                    <th>Image</th>
                    <th>Dialougue</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($site_img_n_logo as $info) {
                echo "<tr>";?>
                <form name="sms_api" action="{{ url('edited_site_image_n_logo') }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" name="id" value="<?php echo $info->id ?>">
                    <td style="width: 20%"><input type="text" disabled="disabled" name="position" value="<?php echo $info->position ?>"></td>
                    <?php if( $info->img==null){?>
                    <td style="width: 10%"><input type="file" name="img"></td>
                    <?php }
                    else{?>
                    <td style="width: 10%">
                        <level><?php echo $info->img ;?></level>
                        <input type="file" required name="img" value="1.png"></td>
                    <?php }?>
                    <td style="width: 60%"><input type="text" required class="form-control" style="width: 100%" name="dialougue" value="<?php echo $info->dialougue ?>"></td>
                    <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                </form>
                <?php }?>
                </tbody>
            </table>
        </div>
            </div>
        <br>
        <div class="box-body">
            <form name="faq" action="{{ url('edited_auto_chat') }}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Desktop auto chat message set</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    echo "<tr>";?>
                    <td colspan="2"><input class="form-control" required type="text" name="msg" placeholder="message"></td>
                    <td><button  type="submit" class="btn btn-info pull-left">Save</button></td>
                    </tbody>
                </table>
            </form>
        </div>
        <br>
        <div class="box-body">
            <form name="faq" action="{{ url('edited_chat_person') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Three chat person Images</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    echo "<tr>";?>
                    <td><input class="form-control" type="file" required name="img_1" placeholder="person_1"></td>
                    <td><input class="form-control" type="file" required name="img_2" placeholder="person_2"></td>
                    <td><input class="form-control" type="file" required name="img_3" placeholder="person_3"></td>
                    <td><button  type="submit" class="btn btn-info pull-left">Save</button></td>
                    </tbody>
                </table>
            </form>
        </div>
        <br>
        <div style="overflow-y:scroll;overflow:auto">
        <div class="box-body">
                <div class="box-header with-border">
                    <h3 class="box-title"><b>Delivery Based Settings</b></h3>
                </div>
            <table class="table table-bordered table-striped">
                <tr>
                    <form name="kg" action="{{ url('edited_dhaka_hours') }}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <th rowspan="3" style="text-align: center;padding-top: 4%">Dhaka</th>
                        <?php foreach ($dhaka_hours as $hrs){
                        $hr1=$hrs->hour_1;
                        $hr2=$hrs->hour_2;}
                        ?>
                    <th colspan="3">Dhaka city <input type="text" required name="hr1" value="<?php echo $hr1; ?>" style="width: 10%"> hours</th>
                    <th colspan="3">Dhaka city <input type="text" required name="hr2" value="<?php echo $hr2; ?>" style="width: 10%"> hours</th>
                        <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                </tr>
                <?php foreach ($dhaka_kg as $kg){
                    $kg1=$kg->kg_1_for_hr_1;
                    $kg2=$kg->kg_2_for_hr_1;
                    $kg3=$kg->kg_3_for_hr_1;
                    $kg4=$kg->kg_1_for_hr_2;
                    $kg5=$kg->kg_2_for_hr_2;
                    $kg6=$kg->kg_3_for_hr_2;
                    }
                ?>
                <tr>
                    <form name="kg2" action="{{ url('edited_dhaka_kg') }}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <td><input type="text" required name="half_kg_1" value="<?php echo $kg1;?>" placeholder="upto 1/2 kg"></td>
                    <td><input type="text" required name="1_kg_1" value="<?php echo $kg2;?>"  placeholder="upto 1 kg"></td>
                    <td><input type="text" required name="2_kg_1" value="<?php echo $kg3;?>" placeholder="upto 2 kg"></td>

                    {{--for 48 hours--}}
                        <td><input type="text" required name="half_kg_2" value="<?php echo $kg4;?>" placeholder="upto 1/2 kg"></td>
                        <td><input type="text" required name="1_kg_2" value="<?php echo $kg5;?>" placeholder="upto 1 kg"></td>
                        <td><input type="text" required name="2_kg_2" value="<?php echo $kg6;?>" placeholder="upto 2 kg"></td>
                        <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                </tr>
                <tr>
                    <?php foreach ($dhaka_val as $kg){
                        $kg_val_1=$kg->kg_val_1_for_hr_1;
                        $kg_val_2=$kg->kg_val_2_for_hr_1;
                        $kg_val_3=$kg->kg_val_3_for_hr_1;
                        $kg_val_4=$kg->kg_val_1_for_hr_2;
                        $kg_val_5=$kg->kg_val_2_for_hr_2;
                        $kg_val_6=$kg->kg_val_3_for_hr_2;
                    }?>
                    <form name="val" action="{{ url('edited_dhaka_kg_val') }}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <td><input type="text" required value="<?php echo $kg_val_1;?>" name="24_val_1"></td>
                    <td><input type="text" required value="<?php echo $kg_val_2;?>" name="24_val_2"></td>
                    <td><input type="text" required value="<?php echo $kg_val_3;?>" name="24_val_3"></td>
                    {{--for 48 hours--}}
                    <td><input type="text" required value="<?php echo $kg_val_4;?>" name="48_val_1"></td>
                    <td><input type="text" required value="<?php echo $kg_val_5;?>" name="48_val_2"></td>
                    <td><input type="text" required value="<?php echo $kg_val_6;?>" name="48_val_3"></td>
                        <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                </tr>
            </table>
        </div>
            </div>
        <br>
        <div style="overflow-y:scroll;overflow:auto">
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <tr>
                        <form name="kg" action="{{ url('edited_other_hours') }}" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <th rowspan="2" style="text-align: center;padding-top: 4%">City</th>
                            <?php foreach ($other_city_hours as $hrs){
                                $hr1=$hrs->hour_1;
                                $hr2=$hrs->hour_2;
                            break;
                            }
                            ?>
                            <th colspan="3">City area <input type="text" required name="hr1" value="<?php echo $hr1; ?>" style="width: 10%"> hours</th>
                            <th colspan="3">Dhaka to city area <input type="text" required name="hr2" value="<?php echo $hr2; ?>" style="width: 10%"> hours</th>
                            <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                    </tr>
                    <?php foreach ($other_city_kg as $kg){
                        $kg1=$kg->kg_1_for_hr_1;
                        $kg2=$kg->kg_2_for_hr_1;
                        $kg3=$kg->kg_3_for_hr_1;
                        $kg4=$kg->kg_1_for_hr_2;
                        $kg5=$kg->kg_2_for_hr_2;
                        $kg6=$kg->kg_3_for_hr_2;
                        break;
                    }
                    ?>
                    <tr>
                        <form name="kg2" action="{{ url('edited_other_kg') }}" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <td><input type="text" required name="half_kg_1" value="<?php echo $kg1;?>" placeholder="upto 1/2 kg"></td>
                            <td><input type="text" required name="1_kg_1" value="<?php echo $kg2;?>"  placeholder="upto 1 kg"></td>
                            <td><input type="text" required name="2_kg_1" value="<?php echo $kg3;?>" placeholder="upto 2 kg"></td>

                            {{--for 48 hours--}}
                            <td><input type="text" required name="half_kg_2" value="<?php echo $kg4;?>" placeholder="upto 1/2 kg"></td>
                            <td><input type="text" required name="1_kg_2" value="<?php echo $kg5;?>" placeholder="upto 1 kg"></td>
                            <td><input type="text" required name="2_kg_2" value="<?php echo $kg6;?>" placeholder="upto 2 kg"></td>
                            <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                    </tr>
                    <tr>
                        <?php foreach ($ctg_city_val as $kg){
                            $kg_val_1=$kg->kg_val_1_for_hr_1;
                            $kg_val_2=$kg->kg_val_2_for_hr_1;
                            $kg_val_3=$kg->kg_val_3_for_hr_1;
                            $kg_val_4=$kg->kg_val_1_for_hr_2;
                            $kg_val_5=$kg->kg_val_2_for_hr_2;
                            $kg_val_6=$kg->kg_val_3_for_hr_2;
                            $city=$kg->city;
                        }?>
                        <form name="val" action="{{ url('edited_ctg_kg_val') }}" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <td><?php echo $city; ?></td>
                            <td><input type="text" required value="<?php echo $kg_val_1;?>" name="24_val_1"></td>
                            <td><input type="text" required value="<?php echo $kg_val_2;?>" name="24_val_2"></td>
                            <td><input type="text" required value="<?php echo $kg_val_3;?>" name="24_val_3"></td>
                            {{--for 48 hours--}}
                            <td><input type="text" required value="<?php echo $kg_val_4;?>" name="48_val_1"></td>
                            <td><input type="text" required value="<?php echo $kg_val_5;?>" name="48_val_2"></td>
                            <td><input type="text" required value="<?php echo $kg_val_6;?>" name="48_val_3"></td>
                            <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                    </tr>
                    <tr>
                        <?php foreach ($raj_city_val as $kg){
                            $kg_val_1=$kg->kg_val_1_for_hr_1;
                            $kg_val_2=$kg->kg_val_2_for_hr_1;
                            $kg_val_3=$kg->kg_val_3_for_hr_1;
                            $kg_val_4=$kg->kg_val_1_for_hr_2;
                            $kg_val_5=$kg->kg_val_2_for_hr_2;
                            $kg_val_6=$kg->kg_val_3_for_hr_2;
                            $city=$kg->city;
                        }?>
                        <form name="val" action="{{ url('edited_raj_kg_val') }}" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <td><?php echo $city; ?></td>
                            <td><input type="text" required value="<?php echo $kg_val_1;?>" name="24_val_1"></td>
                            <td><input type="text" required value="<?php echo $kg_val_2;?>" name="24_val_2"></td>
                            <td><input type="text" required value="<?php echo $kg_val_3;?>" name="24_val_3"></td>
                            {{--for 48 hours--}}
                            <td><input type="text" required value="<?php echo $kg_val_4;?>" name="48_val_1"></td>
                            <td><input type="text" required value="<?php echo $kg_val_5;?>" name="48_val_2"></td>
                            <td><input type="text" required value="<?php echo $kg_val_6;?>" name="48_val_3"></td>
                            <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                    </tr>
                    <tr>
                        <?php foreach ($khul_city_val as $kg){
                            $kg_val_1=$kg->kg_val_1_for_hr_1;
                            $kg_val_2=$kg->kg_val_2_for_hr_1;
                            $kg_val_3=$kg->kg_val_3_for_hr_1;
                            $kg_val_4=$kg->kg_val_1_for_hr_2;
                            $kg_val_5=$kg->kg_val_2_for_hr_2;
                            $kg_val_6=$kg->kg_val_3_for_hr_2;
                            $city=$kg->city;
                        }?>
                        <form name="val" action="{{ url('edited_khul_kg_val') }}" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <td><?php echo $city; ?></td>
                            <td><input type="text" required value="<?php echo $kg_val_1;?>" name="24_val_1"></td>
                            <td><input type="text" required value="<?php echo $kg_val_2;?>" name="24_val_2"></td>
                            <td><input type="text" required value="<?php echo $kg_val_3;?>" name="24_val_3"></td>
                            {{--for 48 hours--}}
                            <td><input type="text" required value="<?php echo $kg_val_4;?>" name="48_val_1"></td>
                            <td><input type="text" required value="<?php echo $kg_val_5;?>" name="48_val_2"></td>
                            <td><input type="text" required value="<?php echo $kg_val_6;?>" name="48_val_3"></td>
                            <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                    </tr>
                    <tr>
                        <?php foreach ($syl_city_val as $kg){
                            $kg_val_1=$kg->kg_val_1_for_hr_1;
                            $kg_val_2=$kg->kg_val_2_for_hr_1;
                            $kg_val_3=$kg->kg_val_3_for_hr_1;
                            $kg_val_4=$kg->kg_val_1_for_hr_2;
                            $kg_val_5=$kg->kg_val_2_for_hr_2;
                            $kg_val_6=$kg->kg_val_3_for_hr_2;
                            $city=$kg->city;
                        }?>
                        <form name="val" action="{{ url('edited_syl_kg_val') }}" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <td><?php echo $city; ?></td>
                            <td><input type="text" required value="<?php echo $kg_val_1;?>" name="24_val_1"></td>
                            <td><input type="text" required value="<?php echo $kg_val_2;?>" name="24_val_2"></td>
                            <td><input type="text" required value="<?php echo $kg_val_3;?>" name="24_val_3"></td>
                            {{--for 48 hours--}}
                            <td><input type="text" required value="<?php echo $kg_val_4;?>" name="48_val_1"></td>
                            <td><input type="text" required value="<?php echo $kg_val_5;?>" name="48_val_2"></td>
                            <td><input type="text" required value="<?php echo $kg_val_6;?>" name="48_val_3"></td>
                            <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                    </tr>
                    <tr>
                        <?php foreach ($overall_city_val as $kg){
                            $kg_val_1=$kg->kg_val_1_for_hr_1;
                            $kg_val_2=$kg->kg_val_2_for_hr_1;
                            $kg_val_3=$kg->kg_val_3_for_hr_1;
                            $kg_val_4=$kg->kg_val_1_for_hr_2;
                            $kg_val_5=$kg->kg_val_2_for_hr_2;
                            $kg_val_6=$kg->kg_val_3_for_hr_2;
                            $city=$kg->city;
                        }?>
                        <form name="val" action="{{ url('edited_overall_kg_val') }}" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <td><?php echo $city; ?></td>
                            <td><input type="text" required value="<?php echo $kg_val_1;?>" name="24_val_1"></td>
                            <td><input type="text" required value="<?php echo $kg_val_2;?>" name="24_val_2"></td>
                            <td><input type="text" required value="<?php echo $kg_val_3;?>" name="24_val_3"></td>
                            {{--for 48 hours--}}
                            <td><input type="text" required value="<?php echo $kg_val_4;?>" name="48_val_1"></td>
                            <td><input type="text" required value="<?php echo $kg_val_5;?>" name="48_val_2"></td>
                            <td><input type="text" required value="<?php echo $kg_val_6;?>" name="48_val_3"></td>
                            <td><button  type="submit" class="btn btn-info pull-left">Update</button></td>
                        </form>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box-body">
            <form name="sms_api" action="{{ url('edited_direction_sms') }}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Delivery direction</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    echo "<tr>";
                            if($delivery_sms!=null){
                    foreach($delivery_sms as $sms){
                    ?>
                    <td><textarea style="height: 150px" class="form-control" required name="delivery_sms" ><?php echo $sms->sms; ?></textarea></td>
                    <?php
                    echo "<tr>";}}
                    else{?>
                    <td><textarea style="height: 150px" class="form-control" required type="text" name="delivery_sms"></textarea></td>
                    <?php }
                    echo "<tr>";?>
                    <td><button  type="submit" class="btn btn-info pull-right">Update</button></td>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <script>
        function average() {

            document.getElementById('average_tk').checked = false;
        }
        function non_average() {

            document.getElementById('non_average').checked = false;
        }
        </script>
    <script>
        function checkother_1() {

            document.getElementById('radio_2').checked = false;
            document.getElementById('radio_3').checked = false;
        }
        function checkother_2() {

            document.getElementById('radio_1').checked = false;
            document.getElementById('radio_3').checked = false;
        }
        function checkother_3() {

            document.getElementById('radio_1').checked = false;
            document.getElementById('radio_2').checked = false;
        }
        function checkoption() {
            if ( ( document.getElementById('radio_1').checked == false ) && ( document.getElementById('radio_2').checked == false ) && ( document.getElementById('radio_3').checked == false ) )
            {
                alert ( "Please choose options for FAQ " );
                return false;
            }
        }


    </script>
    <script>
        function uncheck_1() {

            document.forms["average_refaral"]["average_refaral/1"].checked = false;
        }
        function uncheck_2() {

            document.getElementById('average_refaral/2').checked = false;
        }
        function offcheck_1() {

            document.getElementById('non_average_refaral/1').checked = false;
        }
        function offcheck_2() {

            document.getElementById('non_average_refaral/2').checked = false;
        }

    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')