
@section('content')
    <div class="content-wrapper">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Biker payment</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table" class="table table-bordered table-striped">
                <thead>
                <tr>

                    <th>Biker ID</th>
                    <th>Name</th>
                    <th>Total_trip</th>
                    {{--<th>Full Trip</th>--}}
                    {{--<th>Mis Trip</th>--}}
                    <th>Earn (TK)</th>
                    <th>Company Due (TK)</th>
                    <th>From Due</th>
                    <th>Payment Date</th>
                    <th>User</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($biker_info as $biker) {
                    echo "<tr>";

                    echo "<td>{$biker->id}</td>";
                echo "<td class='tdStyle'>{$biker->biker_name}</td>";
                foreach ($total_trip as $trip_info){
                    if($trip_info->biker_id==$biker->id){?>
                <td><?php echo $trip_info->tp;?></td>
                    <?php }

                }?>
                <?php
                    echo "<td>{$biker->earn}</td>";
                    echo "<td>{$biker->company_due}</td>";
                    echo "<td>{$biker->from_due}</td>";
                    echo "<td>{$biker->payment_date}</td>";
                    echo "<td>{$biker->user}</td>";?>
                <td><a href="{!! url('biker_edit',array('id'=>$biker->id)) !!}" class='btn btn-block btn-success'>Edit</a></td>
                <?}?>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(function () {
            $("#biker_table").DataTable();
        });
    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')