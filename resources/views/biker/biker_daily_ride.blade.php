
@section('content')
    <div class="content-wrapper">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Biker Daily Ride</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Biker ID</th>
                    <th>Name</th>
                    <th>Passenger</th>
                    <th>Time & Date</th>
                    <th>Pick_up & Destination</th>
                    <th>KM</th>
                    <th>Trip</th>
                    <th>Earn (TK)</th>
                    <th>refarel</th>
                    <th>Location</th>
                    <th>User</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                        $serial=0;
                foreach($biker_info as $biker) {
                        $serial++;
                echo "<tr>";

                echo "<td>{$serial}</td>";
                echo "<td>{$biker->biker_id}</td>";
                echo "<td class='tdStyle'>{$biker->biker_name}</td>";
                echo "<td class='tdStyle'>{$biker->passenger_phn}</td>";
                ?>
                <td><?php echo $biker->start_time . ' to '.$biker->end_time .' , '.$biker->date;?></td>
                <td><?php echo $biker->pick_up_area . ' to '.$biker->destination_area;?></td>

                <?php
                echo "<td>{$biker->total_km}</td>";
                echo "<td>{$biker->trip_type}</td>";
                echo "<td>{$biker->earn}</td>";
                echo "<td>{$biker->raferal}</td>";
                echo "<td>{$biker->location}</td>";
                echo "<td>{$biker->user}</td>";?>
                <td><a href="{!! url('biker_daily_ride_details',array('id'=>$biker->biker_id)) !!}" class='btn btn-block btn-success'>Details</a></td>
                <?}?>
                </tbody>
            </table>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Top Ride</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table2" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Biker ID</th>
                    <th>Name</th>
                    <th>Total Trip</th>
                    <th>Full Trip</th>
                    <th>Mis Trip</th>
                    <th>Earn (TK)</th>
                    <th>Time & Date</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $serial=0;
                foreach($top_ride as $info) {
                $serial++;
                echo "<tr>";

                echo "<td>{$serial}</td>";
                echo "<td>{$info->biker_id}</td>";
                echo "<td class='tdStyle'>{$info->biker_name}</td>";
                echo "<td class='tdStyle'>{$info->tp}</td>";
                echo "<td class='tdStyle'>{$info->fp}</td>";
                echo "<td class='tdStyle'>{$info->mp}</td>";
                echo "<td class='tdStyle'>{$info->fare}</td>";
                ?>
                <td><?php echo $info->end_time .' , '.$info->date;?></td>

                <?php
                }?>
                </tbody>
            </table>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Lifetime Earning</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table3" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Passenger</th>
                    <th>Time</th>
                    <th>Pick & Destination</th>
                    <th>Total KM</th>
                    <th>Trip Type</th>
                    <th>Rating</th>
                    <th>Fare(TK)</th>
                    <th>Raferal</th>
                    <th>User</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $serial=0;
                foreach($lifetime_earning as $info) {
                $serial++;
                echo "<tr>";

                echo "<td>{$serial}</td>";
                echo "<td>{$info->passenger_phn}</td>";
                ?>
                <td><?php echo $info->start_time . ' to '.$info->end_time .' , '.$info->date;?></td>
                <td><?php echo $info->pick_up_area . ' to '.$info->destination_area;?></td>

                <?php
                echo "<td class='tdStyle'>{$info->total_km}</td>";
                echo "<td class='tdStyle'>{$info->trip_type}</td>";
                echo "<td class='tdStyle'>{$info->rating}</td>";?>
                <td><?php echo abs($info->fare);?></td>
                <?php
                echo "<td class='tdStyle'>{$info->raferal}</td>";
                echo "<td class='tdStyle'>{$info->user}</td>";
                }?>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(function () {
            $("#biker_table").DataTable();
        });

    </script>
    <script>
        $(function () {
            $("#biker_table2").DataTable();
        });

    </script>
    <script>
        $(function () {
            $("#biker_table3").DataTable();
        });

    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')