
@section('content')
    <div class="content-wrapper">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Biker Daily Ride Details</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Biker ID</th>
                    <th>Name</th>
                    <th>Passenger</th>
                    <th>Time & Date</th>
                    <th>Pick_up & Destination</th>
                    <th>KM</th>
                    <th>Trip</th>
                    <th>Earn (TK)</th>
                    <th>refarel</th>
                    <th>Location</th>
                    <th>User</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                        $serial=0;
                foreach($biker_info as $biker) {
                        $serial++;
                echo "<tr>";

                echo "<td>{$serial}</td>";
                echo "<td>{$biker->biker_id}</td>";
                echo "<td class='tdStyle'>{$biker->biker_name}</td>";
                echo "<td class='tdStyle'>{$biker->passenger_phn}</td>";
                ?>
                <td><?php echo $biker->start_time . ' to '.$biker->end_time .' , '.$biker->date;?></td>
                <td><?php echo $biker->pick_up_area . ' to '.$biker->destination_area;?></td>

                <?php
                echo "<td>{$biker->total_km}</td>";
                echo "<td>{$biker->trip_type}</td>";
                echo "<td>{$biker->earn}</td>";
                echo "<td>{$biker->raferal}</td>";
                echo "<td>{$biker->location}</td>";
                echo "<td>{$biker->user}</td>";?>
                <td><a href="{!! url('biker_daily_ride_details',array('id'=>$biker->biker_id)) !!}" class='btn btn-block btn-success'>Details</a></td>
                <?}?>
                </tbody>
            </table>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title"><b><?php foreach ($biker_name as $name){echo $name->biker_name;} ?> Details</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table2" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Biker ID</th>
                    <th>Passenger</th>
                    <th>Time & Date</th>
                    <th>Pick_up & Destination</th>
                    <th>KM</th>
                    <th>Trip</th>
                    <th>Earn (TK)</th>
                    <th>refarel</th>
                    <th>Location</th>
                    <th>User</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $serial=0;
                foreach($biker_details as $info) {
                $serial++;
                echo "<tr>";

                echo "<td>{$serial}</td>";
                echo "<td>{$info->biker_id}</td>";
                echo "<td class='tdStyle'>{$info->passenger_phn}</td>";
                ?>
                <td><?php echo $info->start_time . ' to '.$info->end_time .' , '.$info->date;?></td>
                <td><?php echo $info->pick_up_area . ' to '.$info->destination_area;?></td>

                <?php
                echo "<td>{$info->total_km}</td>";
                echo "<td>{$info->trip_type}</td>";
                echo "<td>{$info->earn}</td>";
                echo "<td>{$info->raferal}</td>";
                echo "<td>{$info->location}</td>";
                echo "<td>{$info->user}</td>";?>
                <td><a href="{!! url('biker_details_delete',array('id'=>$info->ride_id,'biker_id'=>$info->biker_id)) !!}" class='btn btn-block btn-success'>Delete</a></td>
                <?}?>
                </tbody>
            </table>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Top Ride</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table3" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Biker ID</th>
                    <th>Name</th>
                    <th>Total Trip</th>
                    <th>Full Trip</th>
                    <th>Mis Trip</th>
                    <th>Earn (TK)</th>
                    <th>Time & Date</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $serial=0;
                foreach($top_ride as $info) {
                $serial++;
                echo "<tr>";

                echo "<td>{$serial}</td>";
                echo "<td>{$info->biker_id}</td>";
                echo "<td class='tdStyle'>{$info->biker_name}</td>";
                echo "<td class='tdStyle'>{$info->tp}</td>";
                echo "<td class='tdStyle'>{$info->fp}</td>";
                echo "<td class='tdStyle'>{$info->mp}</td>";
                echo "<td class='tdStyle'>{$info->fare}</td>";
                ?>
                <td><?php echo $info->end_time .' , '.$info->date;?></td>

                <?php
                }?>
                </tbody>
            </table>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Lifetime Earning</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table4" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Passenger</th>
                    <th>Time</th>
                    <th>Pick & Destination</th>
                    <th>Total KM</th>
                    <th>Trip Type</th>
                    <th>Rating</th>
                    <th>Fare(TK)</th>
                    <th>Raferal</th>
                    <th>User</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $serial=0;
                foreach($lifetime_earning as $info) {
                $serial++;
                echo "<tr>";

                echo "<td>{$serial}</td>";
                echo "<td>{$info->passenger_phn}</td>";
                ?>
                <td><?php echo $info->start_time . ' to '.$info->end_time .' , '.$info->date;?></td>
                <td><?php echo $info->pick_up_area . ' to '.$info->destination_area;?></td>

                <?php
                echo "<td class='tdStyle'>{$info->total_km}</td>";
                echo "<td class='tdStyle'>{$info->trip_type}</td>";
                echo "<td class='tdStyle'>{$info->rating}</td>";?>
                <td><?php echo abs($info->fare);?></td>
                <?php
                echo "<td class='tdStyle'>{$info->raferal}</td>";
                echo "<td class='tdStyle'>{$info->user}</td>";
                }?>
                </tbody>
            </table>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title"><b><?php foreach ($biker_name as $name){echo $name->biker_name;} ?> Profile</b></h3>
        </div><br>
        <div class="box-body">
            <ul class="list-inline intro-social-buttons">
                <li>
                    <?php if($image!=null){?> ?>
                    <a href="">
                        <img src="{{ URL::asset('uploaded_image/user_profile/') }}<?$image;?>"  class="img-thumbnail" alt="HTML tutorial" >
                    </a></li><?} else{?>
                <a href="">
                    <img src="{{ URL::asset('uploaded_image/user_profile/default.png') }}"  class="img-thumbnail" alt="HTML tutorial" style="width:350px;height:180px;border:0">
                </a></li><?}?>
                <li>
                    <?php if($nid_img!=null){?> ?>
                    <a href="">
                        <img src="{{ URL::asset('uploaded_image/nid_img/') }}<?$image;?>" class="img-thumbnail" alt="HTML tutorial" >
                    </a></li><?}else{?>
                <a href="">
                    <img src="{{ URL::asset('uploaded_image/nid_img/nid_img.jpg') }}"  class="img-thumbnail" alt="HTML tutorial" style="width:350px;height:180px;border:0">
                </a></li><?}?>
                <li>

                    <?php if($reg_img!=null){?> ?>
                        <a href="">
                        <img src="{{ URL::asset('uploaded_image/reg_img/') }}<?$image;?>" class="img-thumbnail" alt="HTML tutorial" >
                    </a></li><?}else{?>
                <a href="">
                    <img src="{{ URL::asset('uploaded_image/reg_img/office_id.png') }}"  class="img-thumbnail" alt="HTML tutorial" style="width:350px;height:180px;border:0">
                </a></li><?}?>
                </ul>

        </div>
        <div class="box-body">
            <table>
            <form action="{{ url('edited_biker_profile') }}" method="post">
                <?php foreach ($biker_profile as $info){?>
                <input type="hidden" name="id" value="<?php echo $info->id ?>">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <tr>
                    <td>
            <input type="text" style="width: 200px;height: 40px;" name="biker_name" value="<?php echo $info->biker_name ?>"></td>

                    <td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <input type="text" style="width: 200px;height: 40px;" name="biker_phn" value="<?php echo $info->biker_phn ?>"></td>
                    <td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <input type="text" style="width: 200px;height: 40px;" name="bike_name" value="<?php echo $info->bike_name ?>"></td>
                </tr>
                <tr><td><br/></td></tr>
                <tr>

                <td>
            <input type="text" style="width: 200px;height: 40px;" name="nid" value="<?php echo $info->biker_nid ?>"></td>
                <td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <input type="text" style="width: 200px;height: 40px;" name="reg" value="<?php echo $info->biker_reg ?>"></td></tr>
                <tr><td><br/></td></tr>
                <tr>
                    <td>
            <input type="text" style="width: 200px;height: 40px;" name="gender" value="<?php echo $info->gender ?>"></td>
                    <td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        <input type="text" style="width: 200px;height: 40px;" name="location" value="<?php echo $info->location ?>"></td>
                    <td>
            </td></tr>
        <?php }?>
                    <tr><td><br/></td></tr>
                    <tr><td>
                <div class="box-footer">
                    <button  type="submit" class="btn btn-info pull-left">Update Biker Profile</button>
                </div></td></tr>
            </form>
                </table>
        </div>
    </div>
    <script>
        $(function () {
            $("#biker_table").DataTable();
        });
    </script>
    <script>
        $(function () {
            $("#biker_table2").DataTable();
        });
    </script>
    <script>
        $(function () {
            $("#biker_table3").DataTable();
        });
    </script>
    <script>
        $(function () {
            $("#biker_table4").DataTable();
        });
    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')