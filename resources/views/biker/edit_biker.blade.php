@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Biker Payment</h3>
                        </div>

                        <form action="{{ url('edited_biker') }}" method="post" role="form">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                            <div class="box-body">
                                <?php foreach ($biker_info as $info){?>
                                    <input type="hidden" name="id" value="<?php echo $info->id; ?>">
                                <div class="form-group">
                                    <label>Biker Name</label><span style="color: #ff0000">*</span>
                                    <input name="biker_name" required type="text" class="form-control" value="<?php echo $info->biker_name; ?>" >
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Earn</label><span style="color: #ff0000">*</span>
                                    <input name="earn" required type="text" class="form-control" value="<?php echo $info->earn; ?>">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Company Due</label><span style="color: #ff0000">*</span>
                                    <input name="company_due" required type="text" class="form-control" value="<?php echo $info->company_due; ?>">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                    <div class="form-group">
                                        <label>From due</label><span style="color: #ff0000">*</span>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" required name="from_due" id="from_due" class="form-control pull-right" value="<?php echo $info->from_due; ?>" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Payment date</label><span style="color: #ff0000">*</span>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" required name="payment_date" id="payment_date" class="form-control pull-right" value="<?php echo $info->payment_date; ?>" >
                                        </div>
                                    </div>
                                <?php }?>
                                <div class="box-footer">
                                    <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
                                    <button  type="submit" class="btn btn-info pull-right">Update Biker Payment</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function(){
            $('#from_due').datepicker({
                autoclose: true
            });

        });
    </script>
    <script>
        $(document).ready(function(){
            $('#payment_date').datepicker({
                autoclose: true
            });

        });
    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')