
@section('content')
    <div class="content-wrapper">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Passenger</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Pass. Name</th>
                    <th>Biker Phone</th>
                    <th>Time & Date</th>
                    <th>Pick_up & Destination</th>
                    <th>KM</th>
                    <th>Full/Mis Trip</th>
                    <th>Raferal</th>
                    <th>User</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                        $serial=0;
                foreach($passenger_info as $info) {
                        $serial++;
                    echo "<tr>";
                    echo "<td class='tdStyle'>{$serial}</td>";
                    echo "<td class='tdStyle'>{$info->passenger_name}</td>";
                    echo "<td>{$info->biker_phn}</td>";?>

                    <td><?php echo $info->start_time. ' to '.$info->start_time.',  '. $info->date;?></td>
                    <td><?php echo $info->pick_up_area. ' to '.$info->destination_area;?></td>
                <?php
                    echo "<td>{$info->total_km}</td>";
                    echo "<td>{$info->trip_type}</td>";
                    echo "<td>{$info->raferal}</td>";
                    echo "<td>{$info->user}</td>"?>
                <td><a href="{!! url('passenger_details',array('id'=>$info->id)) !!}" class='btn btn-block btn-success'>Details</a></td>
                <?}?>
                </tbody>
            </table>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title"><b>Lifetime Raiding</b></h3>
        </div><br>
        <div class="box-body">
            <table id="biker_table4" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Raider</th>
                    <th>Time</th>
                    <th>Pick & Destination</th>
                    <th>Total KM</th>
                    <th>Trip Type</th>
                    <th>Rating</th>
                    <th>Fare(TK)</th>
                    <th>Raferal</th>
                    <th>User</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $serial=0;
                foreach($lifetime_raiding as $info) {
                $serial++;
                echo "<tr>";

                echo "<td>{$serial}</td>";
                echo "<td>{$info->biker_phn}</td>";
                ?>
                <td><?php echo $info->start_time . ' to '.$info->end_time .' , '.$info->date;?></td>
                <td><?php echo $info->pick_up_area . ' to '.$info->destination_area;?></td>

                <?php
                echo "<td class='tdStyle'>{$info->total_km}</td>";
                echo "<td class='tdStyle'>{$info->trip_type}</td>";
                echo "<td class='tdStyle'>{$info->rating}</td>";?>
                <td><?php echo abs($info->fare);?></td>
                <?php
                echo "<td class='tdStyle'>{$info->raferal}</td>";
                echo "<td class='tdStyle'>{$info->user}</td>";
                }?>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#biker_table').DataTable( {
                "scrollX": true
            } );
        } );
    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')