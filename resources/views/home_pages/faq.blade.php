<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        img {
            width: 100%;
            height: auto;
        }
    </style>
    <style>

        .topnav {
            background-color: #333;
            overflow: hidden;
        }

        /* Style the links inside the navigation bar */
        .topnav a {
            float: right;
            display: block;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        /* Change the color of links on hover */
        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        /* Hide the link that should open and close the topnav on small screens */
        .topnav .icon {
            display: none;
        }
        @media screen and (max-width: 600px) {
            .topnav a:not(:first-child) {display: none;}
            .topnav a.icon {
                float: right;
                display: block;
            }
        }

        @media screen and (max-width: 600px) {
            .topnav.responsive {position: relative;}
            .topnav.responsive .icon {
                position: absolute;
                right: 0;
                top: 0;
            }
            .topnav.responsive a {
                float: none;
                display: block;
                text-align: left;
            }

        }
    </style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dako</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/vendor/font-awesome/css/font-awesome.min.css') }}">

    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/css/agency.min.css') }}">


    <link rel="stylesheet" href="{{ URL::asset('theme/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/skins/_all-skins.min.css') }}">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" integrity="sha384-0s5Pv64cNZJieYFkXYOTId2HMA2Lfb6q2nAcx2n0RTLUnCAoTTsS0nKEO27XyKcY" crossorigin="anonymous"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" integrity="sha384-ZoaMbDF+4LeFxg6WdScQ9nnR1QC2MIRxA1O9KWEXQwns1G8UNyIEZIQidzb0T1fo" crossorigin="anonymous"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

<!-- Navigation -->
<nav  >

        <!-- Brand and toggle get grouped for better mobile display -->




        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="topnav" id="myTopnav">
            <a><img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$home_logo) }}" class="img-responsive img-circle"  style="width: 25%" alt="logo"></a>
            <a href="{{ url('become_rider') }}">Become a rider</a>
            <a href="{{ url('faq') }}">FAQ</a>
            <a href="{{ url('delivery_page') }}">Delivery</a>
            <a href="{{ url('ride') }}">Ride</a>
            <a href="{{ url('/') }}">Home</a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
        </div>

</nav>

<!-- Header -->
<header>
    <div id="bg_container">
	<video width="320" height="240" controls>
  <source src="{{ URL::asset('video/faq.mp4') }}" type="video/mp4">
  <source src="{{ URL::asset('video/faq.mp4') }}" type="video/ogg">
  Your browser does not support the video tag.
</video>
        </header>
<!-- Services Section -->
<section id="services">
    <div class="container">
        <div class="row text-center">
            <h3>FOR VARIFICATION PLEASE ANSWER THE QUESTIONS</h3>
            <div id="content">
                <div class="content_item" style="margin-left: 10%">
                    <div style="overflow-y:scroll;overflow:auto;width: 100%">
                        <form name="faq" method="post" action="{{ url('check_faq_ansr') }}">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <table class="table table-bordered table-striped" >
                        <?php
                        $serial=0;
                        foreach ($faq as $info){
                        $serial++;
                        ?>
                        <thead>
                        <tr>
                            <td style="text-align: left" colspan="3"><?php if($info->question!=null){echo $serial.' '. $info->question;}?></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="text-align: left"><input id="radio1/<?php echo $serial;?>"  type="radio" name="<?php echo $serial;?>" value="1">&nbsp; &nbsp;<?php if($info->option_1!=null){echo $info->option_1;}?></td>
                            <td style="text-align: left"><input id="radio2/<?php echo $serial;?>"  type="radio" name="<?php echo $serial;?>"value="2">&nbsp; &nbsp; <?php if($info->option_2!=null){echo $info->option_2;}?></td>
                            <td style="text-align: left"><input id="radio3/<?php echo $serial;?>"  type="radio" name="<?php echo $serial;?>"value="3">&nbsp; &nbsp; <?php if($info->option_3!=null){echo $info->option_3;}?></td>
                        </tr>
                        </tbody>
                        <?php }?>
                    </table>
                            <input type="submit" value="Try again" class="btn btn-info pull-right" onclick="return validation()">
                            </form>
                        </div>
                    <?php $ansr=session()->get('correct_ansr');
                    ?>
                    <p style="float: right"><?php $c=count($faq);
                        $t=$c-1;
                        $tt=$c-2;
                        if($c==$ansr || $ansr==$t || $ansr==$tt){ echo 'Your a/c is review now. After 6 hours you will find a feedback.';}
                        else{
                            echo 'Your result is '.$ansr.' out of '.$c.'. Please see the training session Video again & participate leter';
                        }
                        ?></p>
                    </div><!--close content_container-->
                </div><!--close content_item-->
            </div>
         </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$footer_logo) }}" class="img-responsive img-circle" alt="" style="width: 20%;float: left">
                <span class="copyright">Copyright &copy; Your Website 2017</span>
            </div>
            <div class="col-md-4">
                <ul>
                    <li>Support [10am-7pm];</li>
                    <li>Phone: 012454585699;</li></ul>
            </div>
            <div class="col-md-1">
                <ul class="list-inline social-buttons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- jQuery -->
<script src="{{ URL::asset('home_page_scripts/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/jqBootstrapValidation.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/contact_me.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/agency.min.js') }}"></script>

<script src="{{ URL::asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ URL::asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ URL::asset('theme/dist/js/app.min.js') }}"></script>
<script src="{{ URL::asset('theme/dist/js/demo.js') }}"></script>

<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }

</script>
<script>
    function validation() {
        <?php
		$i=0;
		foreach($faq as $info){
			$i++;
            ?>
            if ( ( document.getElementById('radio1/<?php echo $i;?>').checked == false ) && ( document.getElementById('radio2/<?php echo $i;?>').checked == false )  && ( document.getElementById('radio3/<?php echo $i;?>').checked == false ))
            {
                alert ( "Please choose answer for  Question <?php echo $i; ?>" );
                return false;
            }
        <?php }?>
    }
</script>

</body>

</html>
