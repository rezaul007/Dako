<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        img {
            width: 100%;
            height: auto;
        }
    </style>
    <style>

        .topnav {
            background-color: #333;
            overflow: hidden;
        }

        /* Style the links inside the navigation bar */
        .topnav a {
            float: right;
            display: block;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        /* Change the color of links on hover */
        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        /* Hide the link that should open and close the topnav on small screens */
        .topnav .icon {
            display: none;
        }
        @media screen and (max-width: 600px) {
            .topnav a:not(:first-child) {display: none;}
            .topnav a.icon {
                float: right;
                display: block;
            }
        }

        @media screen and (max-width: 600px) {
            .topnav.responsive {position: relative;}
            .topnav.responsive .icon {
                position: absolute;
                right: 0;
                top: 0;
            }
            .topnav.responsive a {
                float: none;
                display: block;
                text-align: left;
            }

        }
    </style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dako</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/vendor/font-awesome/css/font-awesome.min.css') }}">

    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/css/agency.min.css') }}">


    <link rel="stylesheet" href="{{ URL::asset('theme/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/skins/_all-skins.min.css') }}">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" integrity="sha384-0s5Pv64cNZJieYFkXYOTId2HMA2Lfb6q2nAcx2n0RTLUnCAoTTsS0nKEO27XyKcY" crossorigin="anonymous"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" integrity="sha384-ZoaMbDF+4LeFxg6WdScQ9nnR1QC2MIRxA1O9KWEXQwns1G8UNyIEZIQidzb0T1fo" crossorigin="anonymous"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

<!-- Navigation -->
<nav  >

        <!-- Brand and toggle get grouped for better mobile display -->




        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="topnav" id="myTopnav">
            <a><img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$home_logo) }}" class="img-responsive img-circle"  style="width: 25%" alt="logo"></a>
            <a href="{{ url('faq') }}">FAQ</a>
            <a href="{{ url('become_rider') }}">Become a rider</a>
            <a href="{{ url('delivery_page') }}">Delivery</a>
            <a href="{{ url('ride') }}">Ride</a>
            <a href="{{ url('/') }}">Home</a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
        </div>

</nav>

<!-- Header -->
<header style="width: 100%;height: 500px;background-image:url('{{ asset('uploaded_image/site_img_n_logo/'.$ride_image) }}');" class="img-responsive">
    <div class="container">
        <div class="intro-text">
            <div style="padding-top: 5%;"><h2 style="color: red"><b><?php if($sms!=null){echo $sms;}?></b></h2>
            </div>
        </div>
    </div>
</header>
<div style="width: 80%;margin-left: 10%">
    <img src="{{ URL::asset('uploaded_image/site_img_n_logo/ride.jpg') }}" alt="image1" />
</div>
<section id="services">
    <div class="container">
        <div class="row text-center">
            <div class="box-success">
                <div id="content">
                    <div class="content_item" style="padding-top: -5%">
                        <h4 style="margin-right: -5%">ONE APP, ALL OF BIKERBD</h4>
                        <div class="content_imagetext">
                            <img src="{{ URL::asset('uploaded_image/site_img_n_logo/home_1.jpg') }}" alt="image1" style="float: left;width: 20%;height: 380px;margin-top: -5%"/>
                            <p><div  style="margin-right: 15%">Why you choise us</div>
                            <div style="margin-right: 15%">With just a few clicks,have a bike pick you up and drop you at your destination</div>
                            </p>
                            <p><div style="margin-right: 15%" >We are up to date</div>
                            <div style="margin-right: 15%">With just a few clicks,have a bike pick you up and drop you at your destination</div>
                            </p>
                        </div><!--close content_imagetext-->
                        <!--close button_small-->
                    </div><!--close content_container-->
                </div><!--close content_item-->
            </div>
            <div></div>
        </div>

    </div>
</section>

    <div class="container">
        <div class="row " style="padding-bottom: 5%">
                <div id="content">
                    <div class="content_item" style="padding-left: 5%;padding-bottom: 5%;margin-left: 5%;margin-top: -5%">
                        <h5>Pricing</h5>
                        <h5>Estimate here</h5>
                        <form>
                            <input type="text" class="form-control" name="pick_up" style="width: 20%" placeholder="set pick up"><br>
                            <input type="text" class="form-control" name="destination" style="width: 20%" placeholder="set destination">
                            <br>
                            <input type="submit" value="sbmit">
                        </form>
                    </div><!--close content_container-->
                </div>
            <div id="map" style="width:400px;height:400px;background:yellow;margin-top:-40% ;margin-left: 50%"></div>
        </div>

    </div>

<section id="team" class="bg-light-gray">
    <div class="container" style="margin-top: -80px">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">OUR COMMUNITY</h2>
                <h3 class="section-subheading text-muted">Massage</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="{{ URL::asset('uploaded_image/chat_persons/'.$person_1) }}" class="img-responsive img-circle" alt="" style="width: 50%">
                    <h4>Sujon Mahmud</h4>
                    <h6>Journalist</h6>
                    <p class="text-muted">Why you choise us
                        With just a few clicks,have a bike pick you up and drop you at your destination
                        We are up to date With just a few clicks,have a bike pick you up and drop you at your destination
                    </p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="{{ URL::asset('uploaded_image/chat_persons/'.$person_2) }}" class="img-responsive img-circle" alt="" style="width: 50%">
                    <h4>Ribul Hossain</h4>
                    <h6>Business Man</h6>
                    <p class="text-muted">Why you choise us
                        With just a few clicks,have a bike pick you up and drop you at your destination
                        We are up to date With just a few clicks,have a bike pick you up and drop you at your destination
                    </p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img  src="{{ URL::asset('uploaded_image/chat_persons/'.$person_3) }}" class="img-responsive img-circle" alt="" style="width: 50%">
                    <h4>Mohammud Ullah</h4>
                    <h6>Job Holder</h6>
                    <p class="text-muted">Why you choise us
                        With just a few clicks,have a bike pick you up and drop you at your destination
                        We are up to date With just a few clicks,have a bike pick you up and drop you at your destination
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$footer_logo) }}" class="img-responsive img-circle" alt="" style="width: 20%;float: left">
                <span class="copyright">Copyright &copy; Your Website 2017</span>
            </div>
            <div class="col-md-4">
                <ul>
                    <li>Support [10am-7pm];</li>
                    <li>Phone: 012454585699;</li></ul>
            </div>
            <div class="col-md-1">
                <ul class="list-inline social-buttons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- jQuery -->
<script src="{{ URL::asset('home_page_scripts/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/jqBootstrapValidation.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/contact_me.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/agency.min.js') }}"></script>

<script src="{{ URL::asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ URL::asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ URL::asset('theme/dist/js/app.min.js') }}"></script>
<script src="{{ URL::asset('theme/dist/js/demo.js') }}"></script>


<!-- Bootstrap Core JavaScript -->


<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>

<!-- Contact Form JavaScript -->
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
    function validation() {
        var x=document.getElementById('area').value
        if(x==0){
            alert ( "Please select your area" );
            return false;
        }
            if ( ( document.getElementById('radio_1').checked == false ) && ( document.getElementById('radio_2').checked == false ))
            {
                alert ( "Please choose your gender" );
                return false;
            }
    }
</script>
<script>
    function myMap() {
        var mapOptions = {
            center: new google.maps.LatLng(23.83, 90.40),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.HYBRID
        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
</body>

</html>
