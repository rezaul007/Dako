<!DOCTYPE html>
<html lang="en">

<head>
    <style>

        .topnav {
            background-color: #333;
            overflow: hidden;
        }

        /* Style the links inside the navigation bar */
        .topnav a {
            float: right;
            display: block;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        /* Change the color of links on hover */
        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        /* Hide the link that should open and close the topnav on small screens */
        .topnav .icon {
            display: none;
        }
        @media screen and (max-width: 600px) {
            .topnav a:not(:first-child) {display: none;}
            .topnav a.icon {
                float: right;
                display: block;
            }
        }

        @media screen and (max-width: 600px) {
            .topnav.responsive {position: relative;}
            .topnav.responsive .icon {
                position: absolute;
                right: 0;
                top: 0;
            }
            .topnav.responsive a {
                float: none;
                display: block;
                text-align: left;
            }

        }
    </style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dako</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/vendor/font-awesome/css/font-awesome.min.css') }}">

    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/css/agency.min.css') }}">


    <link rel="stylesheet" href="{{ URL::asset('theme/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/skins/_all-skins.min.css') }}">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" integrity="sha384-0s5Pv64cNZJieYFkXYOTId2HMA2Lfb6q2nAcx2n0RTLUnCAoTTsS0nKEO27XyKcY" crossorigin="anonymous"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" integrity="sha384-ZoaMbDF+4LeFxg6WdScQ9nnR1QC2MIRxA1O9KWEXQwns1G8UNyIEZIQidzb0T1fo" crossorigin="anonymous"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

<!-- Navigation -->
<nav  >

        <!-- Brand and toggle get grouped for better mobile display -->




        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="topnav" id="myTopnav">
            <a><img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$home_logo) }}" class="img-responsive img-circle"  style="width: 25%" alt="logo"></a>
            <a href="{{ url('become_rider') }}">Become a rider</a>
            <a href="{{ url('faq') }}">FAQ</a>
            <a href="{{ url('delivery_page') }}">Delivery</a>
            <a href="{{ url('ride') }}">Ride</a>
            <a href="{{ url('/') }}">Home</a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
        </div>

</nav>

<!-- Header -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$slider_image_1) }}" alt="Image" style="width: 100%;height: 500px">
            <div class="carousel-caption">
                <!--          <h3>Sell $</h3>
                          <p>Money Money.</p>-->
            </div>
        </div>

        <div class="item">
            <img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$slider_image_2) }}" alt="Image" style="width: 100%;height: 500px">
            <div class="carousel-caption">
                <!--          <h3>More Sell $</h3>
                          <p>Lorem ipsum...</p>-->
            </div>
        </div>

        <div class="item">
            <img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$slider_image_3) }}" alt="Image" style="width: 100%;height: 500px">
            <div class="carousel-caption">
                <!--          <h3>More Sell $</h3>
                          <p>Lorem ipsum...</p>-->
            </div>
        </div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<header>
    <div class="container">
        <div class="intro-text">
        <div style="float: left;margin-top: -300px" >
                <a href=""><img src="{{ URL::asset('uploaded_image/site_img_n_logo/ride_with_us.jpg') }}" alt="image1" style="float: left;width: 350px"/></a>
                <br>
                    <img src="{{ URL::asset('uploaded_image/site_img_n_logo/sign_up.jpg') }}" alt="image1" style="float: left;padding-bottom: 25px;width: 350px"/><br>
            <form class="form-horizontal" style="background-color: wheat" method="post" action="{{ url('add_passenger') }}">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-10">
                            <input type="text"  name="name" required class="form-control" id="inputEmail3" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Phone</label>

                        <div class="col-sm-10">
                            <input type="text" name="phone" required class="form-control" id="inputPassword3" placeholder="Phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Area</label>
                        <select class="form-control" style="width: 50%;margin-left: 20%"  name="area" id="area">
                            <option  value='0'> Select</option>
                            <option  value='banani'> Banani</option>
                            <option  value='uttara' >Uttara</option>
                            <option  value='motijhil' >Motijhil</option>
                            <option  value='mdpur'> Mohommod pur</option>
                            <option  value='mirpur'> Mirpur</option>
                            <option  value='dhanmondi' >Dhanmondi</option>
                            <option  value='old_dhaka' >Old dhaka</option>
                            <option  value='jatrabari'> Jatrabari</option>
                            <option  value='badda' >Badda</option>
                            <option  value='malibag' >Malibag</option>
                        </select>
                    </div>
                    <div class="form-group">
                    <div class="col-sm-10">
                        <input type="radio" name="gender" required id="radio_1" value="male">&nbsp;&nbsp; Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="gender" required id="radio_2" value="female"> &nbsp;&nbsp;FeMale
                    </div>
                        </div>
                    <input type="submit" class="btn btn-info pull-center" onclick="return validation()" value="Sign up"><br><br>
                    <a href=""><button type="submit" class="btn btn-info pull-center">Download App</button></a>
                </div>
                <!-- /.box-body -->
                <!-- /.box-footer -->
            </form>
        </div>
        </div>
    </div>
</header>
<br>
<!-- Services Section -->
<section id="services">
    <div class="container">
        <div class="row text-center">
            <div id="content">
                <div class="content_item">

                    <div class="content_imagetext" style="">
                        <h4 style="margin-right: 30%">ONE APP, ALL OF BIKERBD</h4>
                        <img src="{{ URL::asset('uploaded_image/site_img_n_logo/home_1.jpg') }}" alt="image1" style="float: right;width: 30%;height: 500px;margin-top: -10%"/>
                        <p><div  style="margin:auto;">How you ride</div>
                        <div style="margin:auto">With just a few clicks,have a bike pick you up and drop you at your destination</div>
                        </p>
                        <p><div style="margin:auto" >Use Smoothly</div>
                        <div style="margin:auto">With just a few clicks,have a bike pick you up and drop you at your destination</div>
                        </p>
                    </div><!--close content_imagetext-->
                    <!--close button_small-->
                    </div><!--close content_container-->
                </div><!--close content_item-->
            </div>
        <div class="row text-center">
            <div class="box-success">
            <div id="content">
                <div class="content_item" style="padding: 5%">
                    <h4 style="margin-left: 35%">SEND YOUR PRODUCT</h4>
                    <div class="content_imagetext">
                        <img src="{{ URL::asset('uploaded_image/site_img_n_logo/home_2.jpg') }}" alt="image1" style="float: left;width: 35%;height: 350px;margin-top: -5%"/>
                        <p><div  style="margin:auto">Why you choise us</div>
                        <div style="margin:auto">With just a few clicks,have a bike pick you up and drop you at your destination</div>
                        </p>
                        <p><div style="margin:auto" >We are up to date</div>
                        <div style="margin:auto">With just a few clicks,have a bike pick you up and drop you at your destination</div>
                        </p>
                    </div><!--close content_imagetext-->
                    <!--close button_small-->
                </div><!--close content_container-->
            </div><!--close content_item-->
                </div>
            <div></div>
        </div>
        <div class="row text-center">
            <div class="box-success">
                <div id="content">
                    <div class="content_item" style="padding: 5%">
                        <h4 style="margin-right: 35%">Ride with us</h4>
                        <div class="content_imagetext">
                            <img src="{{ URL::asset('uploaded_image/site_img_n_logo/home_3.jpg') }}" alt="image1" style="float: right;width: 35%;height: 350px;margin-top: -8%"/>
                            <p><div  style="margin:auto">Why you choise us</div>
                            <div style="margin:auto">With just a few clicks,have a bike pick you up and drop you at your destination</div>
                            </p>
                            <p><div style="margin:auto" >We are up to date</div>
                            <div style="margin:auto">With just a few clicks,have a bike pick you up and drop you at your destination</div>
                            </p>
                        </div><!--close content_imagetext-->
                        <!--close button_small-->
                    </div><!--close content_container-->
                </div><!--close content_item-->
            </div>
        </div>

            </div>
        </div>
    </div>
</section>
<section id="team" class="bg-light-gray">
    <div class="container" style="margin-top: -80px">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">OUR COMMUNITY</h2>
                <h3 class="section-subheading text-muted">Massage</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="{{ URL::asset('uploaded_image/chat_persons/'.$person_1) }}" class="img-responsive img-circle" alt="" style="width: 50%">
                    <h4>Sujon Mahmud</h4>
                    <h6>Journalist</h6>
                    <p class="text-muted">Why you choise us
                        With just a few clicks,have a bike pick you up and drop you at your destination
                    We are up to date With just a few clicks,have a bike pick you up and drop you at your destination
                    </p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="{{ URL::asset('uploaded_image/chat_persons/'.$person_2) }}" class="img-responsive img-circle" alt="" style="width: 50%">
                    <h4>Ribul Hossain</h4>
                    <h6>Business Man</h6>
                    <p class="text-muted">Why you choise us
                        With just a few clicks,have a bike pick you up and drop you at your destination
                        We are up to date With just a few clicks,have a bike pick you up and drop you at your destination
                    </p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img  src="{{ URL::asset('uploaded_image/chat_persons/'.$person_3) }}" class="img-responsive img-circle" alt="" style="width: 50%">
                    <h4>Mohammud Ullah</h4>
                    <h6>Job Holder</h6>
                    <p class="text-muted">Why you choise us
                        With just a few clicks,have a bike pick you up and drop you at your destination
                        We are up to date With just a few clicks,have a bike pick you up and drop you at your destination
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$footer_logo) }}" class="img-responsive img-circle" alt="" style="width: 20%;float: left">
                <span class="copyright">Copyright &copy; Your Website 2017</span>
            </div>
            <div class="col-md-4">
                <ul>
                    <li>Support [10am-7pm];</li>
                    <li>Phone: 012454585699;</li></ul>
            </div>
            <div class="col-md-1">
                <ul class="list-inline social-buttons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- jQuery -->
<script src="{{ URL::asset('home_page_scripts/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/jqBootstrapValidation.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/contact_me.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/agency.min.js') }}"></script>

<script src="{{ URL::asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ URL::asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ URL::asset('theme/dist/js/app.min.js') }}"></script>
<script src="{{ URL::asset('theme/dist/js/demo.js') }}"></script>


<!-- Bootstrap Core JavaScript -->


<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>

<!-- Contact Form JavaScript -->
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }

</script>

</body>

</html>
