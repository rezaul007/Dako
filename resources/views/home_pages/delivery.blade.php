<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        img {
            width: 100%;
            height: auto;
        }
    </style>
    <style>

        .topnav {
            background-color: #333;
            overflow: hidden;
        }

        /* Style the links inside the navigation bar */
        .topnav a {
            float: right;
            display: block;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        /* Change the color of links on hover */
        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        /* Hide the link that should open and close the topnav on small screens */
        .topnav .icon {
            display: none;
        }
        @media screen and (max-width: 600px) {
            .topnav a:not(:first-child) {display: none;}
            .topnav a.icon {
                float: right;
                display: block;
            }
        }

        @media screen and (max-width: 600px) {
            .topnav.responsive {position: relative;}
            .topnav.responsive .icon {
                position: absolute;
                right: 0;
                top: 0;
            }
            .topnav.responsive a {
                float: none;
                display: block;
                text-align: left;
            }

        }
    </style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dako</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/vendor/font-awesome/css/font-awesome.min.css') }}">

    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ URL::asset('home_page_scripts/css/agency.min.css') }}">


    <link rel="stylesheet" href="{{ URL::asset('theme/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/skins/_all-skins.min.css') }}">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" integrity="sha384-0s5Pv64cNZJieYFkXYOTId2HMA2Lfb6q2nAcx2n0RTLUnCAoTTsS0nKEO27XyKcY" crossorigin="anonymous"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" integrity="sha384-ZoaMbDF+4LeFxg6WdScQ9nnR1QC2MIRxA1O9KWEXQwns1G8UNyIEZIQidzb0T1fo" crossorigin="anonymous"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

<!-- Navigation -->
<nav  >

        <!-- Brand and toggle get grouped for better mobile display -->




        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="topnav" id="myTopnav">
            <a><img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$home_logo) }}" class="img-responsive img-circle"  style="width: 25%" alt="logo"></a>
            <a href="{{ url('faq') }}">FAQ</a>
            <a href="{{ url('become_rider') }}">Become a rider</a>
            <a href="{{ url('delivery_page') }}">Delivery</a>
            <a href="{{ url('ride') }}">Ride</a>
            <a href="{{ url('/') }}">Home</a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
        </div>

</nav>

<!-- Header -->
<header style="height: 500px;background-image:url('{{ asset('uploaded_image/site_img_n_logo/'.$delivery_image) }}');border-radius: 5px;" class="img-responsive">
    <div class="container">
            <div style="padding-top: 25%;margin: auto"><h2><b style="color: red"><?php if($sms!=null){echo $sms;}?></b></h2>
            </div>
    </div>
</header>
<div style="margin-left: 17%;width: 200%;padding-top: 8%">
    <img src="{{ URL::asset('uploaded_image/site_img_n_logo/delivery1.jpg') }}" alt="image1" style="float: left;width: 35%;height: 350px;margin-top: -2%;"/>
</div>

<br>

<!-- Services Section -->
<section id="services">
    <div class="container">
        <div class="row text-center">
            <h3>CHEAPEST DELIVERY CHARGE</h3>
            <div id="content">
                <div class="content_item">
                    <div style="overflow-y:scroll;overflow:auto">
                    <table class="table table-bordered table-striped">

                        <tr>
                            <td>Dhaka</td>
                            <td>Chittagong</td>
                            <td>Khulna</td>
                            <td>Rajshahi</td>
                            <td>Sylhet</td>
                            <td>Nation Wide</td>
                        </tr>
                        <tr><td colspan="4" style="text-align: left">Timing</td>
                        <td><?php if($kg1!=null){ echo $kg1;} else{echo 'Upto 500 grams';}?></td>
                        <td><?php if($kg1!=null){ echo $kg2;} else{echo 'Between 500 grams & 1 kg';}?></td>
                        <td><?php if($kg1!=null){ echo $kg3;} else{echo 'Between 1kg & 2kg';}?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: left">Standard deliveries (24 hours)</td>
                            <td><?php if($kg_val_1!=null){ echo $kg_val_1.' TK';} else{echo '0 TK';}?> (promo)</td>
                            <td><?php if($kg_val_2!=null){ echo $kg_val_2.' TK';} else{echo '0 TK';}?></td>
                            <td><?php if($kg_val_3!=null){ echo $kg_val_3.' TK';} else{echo '0 TK';}?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: left">Dhaka subarbs * (48 hours)</td>
                            <td><?php if($kg_val_4!=null){ echo $kg_val_4.' TK';} else{echo '0 TK';}?></td>
                            <td><?php if($kg_val_5!=null){ echo $kg_val_5.' TK';} else{echo '0 TK';}?></td>
                            <td><?php if($kg_val_6!=null){ echo $kg_val_6.' TK';} else{echo '0 TK';}?></td>
                        </tr>

                    </table>
                        </div>
                    </div>
                <div>
                    <?php foreach ($delivery_direction as $sms){?>
                    <textarea disabled class="form-control"/><?php echo $sms->sms; ?></textarea><br>
                    <?php }?>
                </div><!--close content_item-->
            </div>
        <div style="margin-left: 3%;margin-top: 3%">
            <img src="{{ URL::asset('uploaded_image/site_img_n_logo/delivery2.jpg') }}" alt="image1" style="float: left;"/>
        </div>

        <div style="margin-top: 25%">
            <div style="text-align: center">
            <h3 ><b>Register now</b></h3>
        </div>
            <form name="tracking_area" action="{{ url('') }}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div style="overflow-y:scroll;overflow:auto">
                <table id="biker_table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <td colspan="6">Submit company details</td>
                    </tr>
                    <tr>
                        <td>Company name with phone</td>
                        <td>Owner name,mail with phone</td>
                        <td>Wallette/ac no.</td>
                        <td>Office address</td>
                        <td>Site/FB url</td>
                        <td>near our hub</td>
                        <td>Submit</td>
                    </tr>
                    </thead>
                    <tbody>
                    <td><input type="text" name="company_name_n_phone" ></td>
                    <td><input type="text" required name="owner_name_n_mail_n_phone" ></td>
                    <td><input type="text" name="account_no" ></td>
                    <td><input type="text" required name="office_address" ></td>
                    <td><input type="text" name="url" ></td>
                    <td><input type="text" required name="area" ></td>
                    <td><button  type="submit" class="btn btn-info pull-right">Update</button></td>
                    </tbody>
                </table>
                    </div>
            </form>
        </div>



            </div>
        </div>
    </div>
</section>
<section id="team" class="bg-light-gray">
    <div class="container" style="margin-top: -80px">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">OUR COMMUNITY</h2>
                <h3 class="section-subheading text-muted">Massage</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="{{ URL::asset('uploaded_image/chat_persons/'.$person_1) }}" class="img-responsive img-circle" alt="" style="width: 50%">
                    <h4>Sujon Mahmud</h4>
                    <h6>Journalist</h6>
                    <p class="text-muted">Why you choise us
                        With just a few clicks,have a bike pick you up and drop you at your destination
                        We are up to date With just a few clicks,have a bike pick you up and drop you at your destination
                    </p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="{{ URL::asset('uploaded_image/chat_persons/'.$person_2) }}" class="img-responsive img-circle" alt="" style="width: 50%">
                    <h4>Ribul Hossain</h4>
                    <h6>Business Man</h6>
                    <p class="text-muted">Why you choise us
                        With just a few clicks,have a bike pick you up and drop you at your destination
                        We are up to date With just a few clicks,have a bike pick you up and drop you at your destination
                    </p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img  src="{{ URL::asset('uploaded_image/chat_persons/'.$person_3) }}" class="img-responsive img-circle" alt="" style="width: 50%">
                    <h4>Mohammud Ullah</h4>
                    <h6>Job Holder</h6>
                    <p class="text-muted">Why you choise us
                        With just a few clicks,have a bike pick you up and drop you at your destination
                        We are up to date With just a few clicks,have a bike pick you up and drop you at your destination
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="{{ URL::asset('uploaded_image/site_img_n_logo/'.$footer_logo) }}" class="img-responsive img-circle" alt="" style="width: 20%;float: left">
                <span class="copyright">Copyright &copy; Your Website 2017</span>
            </div>
            <div class="col-md-4">
                <ul>
                    <li>Support [10am-7pm];</li>
                    <li>Phone: 012454585699;</li></ul>
            </div>
            <div class="col-md-1">
                <ul class="list-inline social-buttons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- jQuery -->
<script src="{{ URL::asset('home_page_scripts/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/jqBootstrapValidation.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/contact_me.js') }}"></script>
<script src="{{ URL::asset('home_page_scripts/js/agency.min.js') }}"></script>

<script src="{{ URL::asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ URL::asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ URL::asset('theme/dist/js/app.min.js') }}"></script>
<script src="{{ URL::asset('theme/dist/js/demo.js') }}"></script>


<!-- Bootstrap Core JavaScript -->


<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>

<!-- Contact Form JavaScript -->
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
    function validation() {
        var x=document.getElementById('area').value
        if(x==0){
            alert ( "Please select your area" );
            return false;
        }
            if ( ( document.getElementById('radio_1').checked == false ) && ( document.getElementById('radio_2').checked == false ))
            {
                alert ( "Please choose your gender" );
                return false;
            }
    }
</script>

</body>

</html>
