
@section('content')
    <div class="content-wrapper">
        <div class="box-body">
            <div class="box-header">
                <h3 class="box-title" style="text-align: center; font-size: 20px">Stuff List</h3>
                <a href="{!! url('add_stuff') !!}" ><button class="btn btn-info pull-right">Add Stuff</button></a>
            </div>

            <table id="biker_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Stuff Name</th>
                    <th>Permission</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Entry Date</th>
                    <th>Modify Date</th>
                    <th>Phone</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($stuff_info as $info) {
                echo "<tr>";
                echo "<td class='tdStyle'>{$info->stuff_name}</td>";
                echo "<td>{$info->permission}</td>";
                echo "<td>{$info->stuff_type}</td>";?>

                <td><?php if($info->status==1){echo 'Active';} else {echo 'Deactive';};?></td>
                <?php
                echo "<td>{$info->entry_date}</td>";
                echo "<td>{$info->modify_date}</td>";
                echo "<td>{$info->stuff_phn}</td>";?>
                <td><a href="{!! url('stuff_edit',array('id'=>$info->id)) !!}" class='btn btn-block btn-success'>Edit</a></td>
                <td><a href="{!! url('stuff_delete',array('id'=>$info->id)) !!}" class='btn btn-block btn-success'>Delete</a></td>
               <?php

                }?>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(function () {
            $("#biker_table").DataTable();
        });
    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')