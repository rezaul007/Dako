@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add a Stuff</h3>
                        </div>
                        <?if ($error_msg!=null){?>
                        <div class="box-body">
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i><? echo $error_msg;?></h4>
                            </div>
                        </div>
                        <?} ?>
                        <?if ($msg!=null){?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i><? echo $msg;?></h4>
                        </div>
                        <?}?>
                        <form name="stuff" action="{{ url('add_stuff') }}" method="post" role="form" onsubmit="return validateproduct()">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="box-body">



                                <div class="form-group">
                                    <label>Stuff Name</label><span style="color: #ff0000">*</span>
                                    <input name="stuff_name" required type="text" class="form-control" placeholder="Example: Jannaty" value="">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Permission</label><span style="color: #ff0000">*</span>
                                    <input name="permission" required type="text" class="form-control" placeholder="Example: 5 items">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Stuff Type</label><span style="color: #ff0000">*</span>
                                    <input name="stuff_type" required type="text" class="form-control" placeholder="Example: Editor">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Status</label><span style="color: #ff0000">*</span>
                                    <select class="form-control"  name="status">
                                        <option  value=0> Select</option>
                                        <option  value='1' >Active</option>
                                        <option  value='2' >Deactive</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Phone No.</label><span style="color: #ff0000">*</span>
                                    <input type="text" name="phone" required class="form-control" placeholder="Example: +8801716768867">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Password</label><span style="color: #ff0000">*</span>
                                    <input type="text" name="password" required class="form-control" placeholder="Password">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                <div class="box-footer">
                                    <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
                                    <button  type="submit" class="btn btn-info pull-right">Add Stuff</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        function validateproduct() {
            var status = document.forms["stuff"]["status"].value;

            if(status==0){
                alert("Status is not selected");
                return false;
            }
        }
    </script>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')