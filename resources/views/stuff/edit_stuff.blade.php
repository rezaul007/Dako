@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit a Stuff</h3>
                        </div>

                        <form action="{{ url('edited_stuff') }}" method="post" role="form">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <input type="hidden" name="id" value="<?php echo $stuff_id; ?>">
                            <div class="box-body">
                                <?php foreach ($stuff_info as $info){?>
                                <div class="form-group">
                                    <label>Stuff Name</label><span style="color: #ff0000">*</span>
                                    <input name="stuff_name" required type="text" class="form-control" value="<?php echo $info->stuff_name; ?>" >
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Permission</label><span style="color: #ff0000">*</span>
                                    <input name="permission" required type="text" class="form-control" value="<?php echo $info->permission; ?>">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Stuff Type</label><span style="color: #ff0000">*</span>
                                    <input name="stuff_type" required type="text" class="form-control" value="<?php echo $info->stuff_type; ?>">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Status</label><span style="color: #ff0000">*</span>
                                    <select class="form-control"  name="status">
                                        <?php if($info->status==1) {?>
                                        <option  value='1' selected>Active</option>
                                            <option  value='2' >Deactive</option><?php }
                                        else{?>
                                            <option  value='1'  >Active</option>
                                        <option  value='2' selected >Deactive</option>
                                            <?php }?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Phone No.</label><span style="color: #ff0000">*</span>
                                    <input type="text" name="phone" required class="form-control" value="<?php echo $info->stuff_phn; ?>">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="text" name="password"  class="form-control" placeholder="Password">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                    <?php }?>
                                <div class="box-footer">
                                    <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
                                    <button  type="submit" class="btn btn-info pull-right">Update Stuff</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@extends('layouts.footer_page')
@extends('layouts.menu')
@extends('layouts.header_page')